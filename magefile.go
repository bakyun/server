//+build mage

package main

import (
	"io/ioutil"
	"path/filepath"
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/target"

	"gitlab.com/bakyun/magician"
)

var (
	isWin     = runtime.GOOS == "windows"
	tags      = ""
	generated = false
	Default   = Dev
)

func build(app string) error {
	exe := "out/" + app
	if isWin {
		exe += ".exe"
	}

	if !generated {
		magician.Exec("go", "generate", "./...")
		generated = true
	}

	files, err := ioutil.ReadDir(".")
	var paths []string
	for _, f := range files {
		if f.IsDir() && (f.Name() == "out" || f.Name()[0] == byte('.')) {
			continue
		}

		if !f.IsDir() && filepath.Ext(f.Name()) != ".go" {
			continue
		}

		paths = append(paths, f.Name())
	}

	build, err := target.Dir(exe, paths...)
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}

	args := []string{
		"build", "-tags", tags, "-race",
		"-o", exe,
		"-ldflags", magician.GitVersionLDFlags(magician.GetGoPackageName("./config")),
	}
	if mg.Verbose() {
		args = append(args, "-v")
	}

	args = append(args, "./cmd/"+app)

	return magician.Exec("go", args...)

}

func Build() error {
	magician.Headline("Build")
	return build("bakyun")
}

func BuildDaemon() error {
	magician.Headline("Build Daemon")
	return build("bakyund")
}

func Dev() {
	magician.Headline("Dev Mode")
	magician.Echo("enabling dev mode...")
	tags += " debug"
	mg.SerialDeps(Build, BuildDaemon)
}
