package routes

// User routes
const (
	UserProfile = "user.profile"
)

// ActivityPub routes
const (
	APUserProfile   = "ap.user.profile"
	APUserInbox     = "ap.user.inbox"
	APUserOutbox    = "ap.user.outbox"
	APUserFollower  = "ap.user.follower"
	APUserFollowing = "ap.user.following"
	APInbox         = "ap.inbox"
)

// webfinger routes
const (
	NodeInfo = "nodeinfo.infopath"
)

// opauth routes
const (
	OAuthAuthorize = "oauth.authorize"
	OAuthAccess    = "oauth.access"
)
