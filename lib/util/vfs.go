package util

import (
	"net/url"

	"github.com/c2fo/vfs"
	"github.com/c2fo/vfs/vfssimple"
	"gitlab.com/bakyun/server/config"
)

// ResolveVFS resolves vfs references in the db
func ResolveVFS(f vfs.File, cfg *config.Config) (*url.URL, error) {
	uri, err := url.Parse(f.URI())
	if err != nil {
		return nil, err
	}
	if uri.Scheme != "s3" {
		uri.Scheme = "file"
	}

	var base *url.URL

	switch uri.Scheme {
	case "s3":
		base, err = url.Parse(cfg.Store.S3.PublicPath)
	case "file":
		base, err = url.Parse(cfg.Store.FileSystem.PublicPath)
	}

	if err != nil {
		return nil, err
	}

	p := uri.RawPath
	if p[0] == '/' {
		p = p[1:]
	}

	return base.ResolveReference(&url.URL{Path: p}), nil
}

// ResolveVFSString resolves vfs string references in the db
func ResolveVFSString(f string, cfg *config.Config) (*url.URL, error) {
	file, err := vfssimple.NewFile(f)
	if err != nil {
		return nil, err
	}
	return ResolveVFS(file, cfg)
}
