package util

import (
	"fmt"

	"github.com/cking/shutdown"
)

// AddShutdownHook adds a shutdown hook to github.com/cking/shutdown
func AddShutdownHook(fn func() error, ignoreError ...bool) {
	ignore := false
	if len(ignoreError) > 0 {
		ignore = ignoreError[0]
	}

	shutdown.Add(func() {
		err := fn()
		if !ignore && err != nil {
			fmt.Printf("Error during shutdown hook: %v\n", err)
		}
	})
}
