package util

import "time"

var (
	startTime = time.Now()
)

// GetRuntime returns the runtime of the application
func GetRuntime() time.Duration {
	return time.Now().Sub(startTime)
}
