package util

import (
	"net/http"
	"strings"
)

// UpdateCookies forces to update the request cookies with the set-cookie headers from the writer
func UpdateCookies(w http.ResponseWriter, r *http.Request) {
	cookies := strings.Join(w.Header()["Set-Cookie"], ";")
	header := http.Header{}
	header.Add("Cookie", cookies)
	nr := http.Request{Header: header}

	// fix forwarding...
	for _, c := range nr.Cookies() {
		r.AddCookie(c)
	}
}
