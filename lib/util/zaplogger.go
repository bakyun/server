package util

import (
	"log"

	"go.uber.org/zap"
)

type zapWriter struct {
	log func(string, ...zap.Field)
}

func (w *zapWriter) Write(p []byte) (int, error) {
	w.log(string(p))
	return len(p), nil
}

// NewZapLogger creates a log.Logger instance that wraps zap.Logger
func NewZapLogger(logger *zap.Logger, fields ...zap.Field) *log.Logger {
	l := logger.With(fields...)
	return log.New(&zapWriter{l.Debug}, "", 0)
}
