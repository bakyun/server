package chix

import (
	"net/http"

	"github.com/go-chi/chi/middleware"
	"go.uber.org/zap"
)

// ZapFormatter implements chi/middleware.LogFormatter for zap logger
type ZapFormatter struct {
	l *zap.Logger
}

// NewZapFormatter returns a new instance
func NewZapFormatter(l *zap.Logger) *ZapFormatter {
	return &ZapFormatter{l: l}
}

// NewLogEntry creates a new ZapLogEntry
func (z *ZapFormatter) NewLogEntry(r *http.Request) middleware.LogEntry {
	fields := []zap.Field{
		zap.Bool("request_https", isHTTPS(r)),
		zap.String("request_host", r.Host),
		zap.String("request_uri", r.RequestURI),
		zap.String("request_method", r.Method),

		zap.String("remote_addr", r.RemoteAddr),
	}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		fields = append(fields, zap.String("request_id", reqID))
	}

	return &ZapLogEntry{
		l: z.l.With(fields...),
		r: r,
	}
}
