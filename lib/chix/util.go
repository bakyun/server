package chix

import "net/http"

var (
	headerXHTTPS          = http.CanonicalHeaderKey("X-HTTPS")
	headerXForwardedProto = http.CanonicalHeaderKey("X-Forwarded-Proto")
)

func isHTTPS(r *http.Request) bool {
	// nginx reverse proxy
	if r.Header.Get(headerXForwardedProto) == "https" {
		return true
	}

	// apache reverse proxy
	if r.Header.Get(headerXHTTPS) != "" {
		return true
	}

	// no reverse proxy
	if r.TLS != nil {
		return true
	}

	return false
}
