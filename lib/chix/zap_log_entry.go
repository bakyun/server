package chix

import (
	"net/http"
	"time"

	"golang.org/x/xerrors"

	"gitlab.com/bakyun/server/lib/zapx"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// ZapLogEntry for ZapFormatter
type ZapLogEntry struct {
	l     *zap.Logger
	r     *http.Request
	Level *zapcore.Level
}

// Write the current request
func (z *ZapLogEntry) Write(status int, bytes int, elapsed time.Duration) {
	fields := []zap.Field{
		zap.Int("response_status", status),
		zap.String("response_text", http.StatusText(status)),
		zapx.HumanSize("response_size", uint64(bytes)),
		zap.Duration("response_duration", elapsed),
	}

	if z.Level == nil {
		lvl := zapcore.InfoLevel
		z.Level = &lvl
	}

	if ce := z.l.Check(*z.Level, "request complete"); ce != nil {
		ce.Write(fields...)
	}
}

// Panic the current request
func (z *ZapLogEntry) Panic(v interface{}, stack []byte) {
	err, ok := v.(error)
	if !ok {
		err = xerrors.Errorf("%+v", v)
	}

	z.l = z.l.With(
		zap.Error(err),
		zap.ByteString("stack", stack),
	)

	panicLevel := zapcore.PanicLevel
	z.Level = &panicLevel
}
