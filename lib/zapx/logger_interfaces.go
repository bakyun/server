package zapx

import (
	"github.com/jackc/pgx"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Logger that implements various interfaces
type Logger struct {
	*zap.Logger
}

// New zapx Logger wrapper
func New(logger *zap.Logger) *Logger {
	l := new(Logger)
	l.Logger = logger
	return l
}

// Log a message at the given level with data key/value pairs. data may be nil.
func (l *Logger) Log(level pgx.LogLevel, msg string, data map[string]interface{}) {
	lvl := zapcore.DebugLevel

	switch {
	case level == pgx.LogLevelInfo:
		lvl = zapcore.InfoLevel
	case level == pgx.LogLevelWarn:
		lvl = zapcore.WarnLevel
	case level == pgx.LogLevelError:
		lvl = zapcore.ErrorLevel
	}

	if ce := l.Check(lvl, msg); ce != nil {
		fields := make([]zapcore.Field, len(data))
		i := 0
		for k, v := range data {
			fields[i] = zap.Any(k, v)
			i++
		}

		ce.Write(fields...)
	}
}
