package zapx

import (
	"github.com/dustin/go-humanize"
	"go.uber.org/zap/zapcore"
)

// HumanSize converts val into a humanized bytes string
func HumanSize(key string, val uint64) zapcore.Field {
	return zapcore.Field{
		Key:    key,
		Type:   zapcore.StringType,
		String: humanize.IBytes(val),
	}
}
