package controller

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"gitlab.com/bakyun/server/lib/util"

	"gitlab.com/bakyun/server/internal/components/oauth"

	"github.com/asaskevich/govalidator"
	"github.com/go-playground/form"
	"github.com/gorilla/csrf"
	"github.com/openshift/osin"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/internal/services"
	"gitlab.com/bakyun/server/model"
	"gitlab.com/bakyun/server/templates"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
)

// OAuthController stores the controller metadata for oauth requests
type OAuthController struct {
	Logger *zap.SugaredLogger
	Config *config.Config
	DB     *reform.DB
	Osin   *osin.Server
}

// NewOAuthController creates a new OAuthController instance
func NewOAuthController(logger *zap.SugaredLogger) *OAuthController {
	c := new(OAuthController)

	c.Logger = logger
	c.Config = services.GetConfig()
	c.DB = services.GetReformDB()
	c.Osin = oauth.GetServer()

	return c
}

// OAuthAuthorizeContext is the template context for the OAuth authorize handlers
type OAuthAuthorizeContext struct {
	CSRF     string
	Errors   FormErrors
	App      *model.Application
	Account  *model.Account
	Accounts []*model.Account
	Scopes   []string
}

type oauthLoginForm struct {
	Username string `form:"username,omitempty" valid:"required"`
	Password string `form:"password,omitempty" valid:"required"`
}

// PostAuthorize handles the POST authorize endpoint
func (c *OAuthController) PostAuthorize(w http.ResponseWriter, r *http.Request) {
	store := services.GetSession().Load(r)
	ok, err := store.Exists("user")
	if err != nil {
		c.Logger.Errorw("failed to fetch session",
			"err", err,
		)

		Error(w, r.WithContext(context.WithValue(r.Context(), errorValue, ErrorRequest(c.Config, http.StatusInternalServerError, err, "failed to fetch session"))))
		return
	}

	resp := c.Osin.NewResponse()
	defer resp.Close()

	if ar := c.Osin.HandleAuthorizeRequest(resp, r); ar != nil {
		if r.PostFormValue("ok") != "1" {
			ar.Authorized = false
		} else if !ok {
			f := new(oauthLoginForm)
			fe := make(FormErrors)

			if err := form.NewDecoder().Decode(f, r.PostForm); err != nil {
				c.Logger.Errorw("failed to decode post form",
					"err", err,
				)
				fe.AddError("", "Unkown error occured")
			}

			if fe.Valid() {
				ok, err := govalidator.ValidateStruct(f)

				if !ok {
					for f, m := range govalidator.ErrorsByField(err) {
						fe.AddError(f, m)
					}
				}
			}

			if fe.Valid() {
				u, err := model.UserByEmail(c.DB, f.Username)
				if err != nil {
					fe.AddError("username", "User not found")
				} else if !u.VerifyPassword(f.Password) {
					fe.AddError("password", "Invalid password")
				} else {
					if err := store.PutString(w, "user", u.ID); err != nil {
						c.Logger.Errorw("failed to save session",
							"err", err,
						)
						fe.AddError("", "Unkown error occured")
					}
				}
			}

			util.UpdateCookies(w, r)
			c.GetAuthorize(w, SetFormErrors(fe, r))
			return
		} else {
			ar.Authorized = true
		}

		c.Osin.FinishAuthorizeRequest(resp, r, ar)

		if resp.URL == "urn:///" {
			if resp.IsError && resp.InternalError == nil {
				err := errors.New(resp.ErrorId)
				Error(
					w,
					r.WithContext(context.WithValue(
						r.Context(), errorValue,
						ErrorRequest(c.Config, http.StatusForbidden, err, "Authorization for "+ar.Client.GetUserData().(*model.Application).Name+" denied!"),
					)),
				)
				return
			} else if !resp.IsError {
				var ctx struct {
					Code string
					App  *model.Application
				}

				ctx.Code = resp.Output["code"].(string)
				ctx.App = ar.Client.GetUserData().(*model.Application)

				if err := templates.Render(w, "oauth_obb_code.jet", c.Config, ctx); err != nil {
					c.Logger.Errorw(
						"render error",
						"error", err,
					)
				}
				return
			}
		}
	}

	if resp.InternalError != nil {
		c.Logger.Errorw(
			"internal oauth error",
			"error", resp.InternalError,
		)
	}

	if err := osin.OutputJSON(resp, w, r); err != nil {
		c.Logger.Errorw(
			"failed to render oauth response",
			"error", err,
		)
	}
}

// GetAuthorize handles the GET authorize endpoint
func (c *OAuthController) GetAuthorize(w http.ResponseWriter, r *http.Request) {
	store := services.GetSession().Load(r)
	ok, err := store.Exists("user")
	if err != nil {
		c.Logger.Error("failed to fetch session",
			"err", err,
		)

		Error(w, r.WithContext(context.WithValue(r.Context(), errorValue, ErrorRequest(c.Config, http.StatusInternalServerError, err, "failed to fetch session"))))
		return
	}

	resp := c.Osin.NewResponse()
	defer resp.Close()

	if ar := c.Osin.HandleAuthorizeRequest(resp, r); ar != nil {
		ctx := new(OAuthAuthorizeContext)
		ctx.CSRF = string(csrf.TemplateField(r))

		if !ok {
			ctx.Errors, _ = GetFormErrors(r)

			if err := templates.Render(w, "login.jet", c.Config, ctx); err != nil {
				c.Logger.Errorw(
					"render error",
					"error", err,
				)
			}
			return
		}

		if ar.Scope == "" {
			ar.Scope = "read"
		}
		ctx.Scopes = strings.Split(ar.Scope, " ")
		ctx.App = ar.Client.GetUserData().(*model.Application).Configure(c.Config)
		uid, _ := store.GetString("user")
		ctx.Account, err = model.AccountForUser(c.DB, uid)
		if err != nil {
			c.Logger.Error("failed to fetch account ",
				"err", err,
			)

			Error(w, r.WithContext(context.WithValue(r.Context(), errorValue, ErrorRequest(c.Config, http.StatusInternalServerError, err, "failed to fetch session"))))
			return
		}

		ctx.Account.Configure(c.Config)
		ctx.Accounts, err = model.AccountsForUser(c.DB, uid, c.Config)
		if err != nil {
			c.Logger.Error("failed to fetch account ",
				"err", err,
			)

			Error(w, r.WithContext(context.WithValue(r.Context(), errorValue, ErrorRequest(c.Config, http.StatusInternalServerError, err, "failed to fetch session"))))
			return
		}

		if err := templates.Render(w, "authorize.jet", c.Config, ctx); err != nil {
			c.Logger.Errorw(
				"render error",
				"error", err,
			)
		}
		return
	}

	if resp.InternalError != nil {
		c.Logger.Errorw(
			"internal oauth error",
			"error", resp.InternalError,
		)
	}

	if err := osin.OutputJSON(resp, w, r); err != nil {
		c.Logger.Errorw(
			"failed to render oauth response",
			"error", err,
		)
	}
}

// HandleToken handles the token endpoint
func (c *OAuthController) HandleToken(w http.ResponseWriter, r *http.Request) {
	resp := c.Osin.NewResponse()
	defer resp.Close()

	if ar := c.Osin.HandleAccessRequest(resp, r); ar != nil {
		ar.Authorized = true
		c.Osin.FinishAccessRequest(resp, r, ar)
	}

	if resp.InternalError != nil {
		c.Logger.Errorw(
			"internal oauth error",
			"error", resp.InternalError,
		)
	}

	osin.OutputJSON(resp, w, r)
}
