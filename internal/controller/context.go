package controller

import (
	"context"
	"net/http"

	"gitlab.com/bakyun/server/config"
)

type contextKey string

// the different
const (
	errorValue contextKey = "error"
	formValue  contextKey = "form"
)

// ErrorStruct holds the required information for error pages
type ErrorStruct struct {
	Code    int
	Message string
	Error   error
	Stack   bool
	JSON    bool
	Config  *config.Config
	Trace   string
}

// FormErrors hols form validation errors
type FormErrors map[string][]string

// HasErrors checks if a field has errors
func (fe FormErrors) HasErrors(field string) bool {
	f, ok := fe[field]
	if !ok {
		return false
	}

	return len(f) > 0
}

// Valid checks if no errors are set
func (fe FormErrors) Valid() bool {
	for field := range fe {
		if fe.HasErrors(field) {
			return false
		}
	}

	return true
}

// AddError adds an error message to a field
func (fe FormErrors) AddError(field, message string) {
	if _, ok := fe[field]; !ok {
		fe[field] = []string{}
	}

	fe[field] = append(fe[field], message)
}

// SetFormErrors adds a FormErrors instance to a request context
func SetFormErrors(fe FormErrors, r *http.Request) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), formValue, fe))
}

// GetFormErrors returns a FormErrors instance from the request context
func GetFormErrors(r *http.Request) (FormErrors, bool) {
	i := r.Context().Value(formValue)

	if i == nil {
		return make(FormErrors), false
	}

	fe, ok := i.(FormErrors)
	if !ok {
		fe = make(FormErrors)
	}

	return fe, ok
}
