package controller

import (
	"encoding/json"
	"net/http"
	"runtime"
	"runtime/debug"
	"time"

	sigar "github.com/cloudfoundry/gosigar"

	"github.com/dustin/go-humanize"
	"gitlab.com/bakyun/server/lib/util"
)

type statsJSON struct {
	Runtime interface{}
	Memory  struct {
		HeapAlloc interface{}
		HeapSys   interface{}
	}
	Go struct {
		CGoCalls   int64
		GoRoutines int
		Compiler   string
		GCLast     interface{}
		GCs        int64
		GCPause    interface{}
	}
	Sys struct {
		OS     string
		Memory interface{}
		CPUs   int
		Swap   interface{}
		Load   []float64
		Uptime interface{}
	}
}

// Stats returns stats for the server
//
// This is only enabled in debug mode, because it reveals a lot of system
// information without being behind any kind of authentication
func Stats(w http.ResponseWriter, r *http.Request) {
	memStats := new(runtime.MemStats)
	runtime.ReadMemStats(memStats)

	gcStats := new(debug.GCStats)
	debug.ReadGCStats(gcStats)

	s := new(sigar.ConcreteSigar)
	la, _ := s.GetLoadAverage()
	mem, _ := s.GetMem()
	swap, _ := s.GetSwap()

	uptime := new(sigar.Uptime)
	uptime.Get()

	stats := new(statsJSON)
	stats.Go.CGoCalls = runtime.NumCgoCall()
	stats.Go.GoRoutines = runtime.NumGoroutine()
	stats.Go.Compiler = runtime.Version()
	stats.Go.GCs = gcStats.NumGC
	stats.Sys.OS = runtime.GOOS
	stats.Sys.CPUs = runtime.NumCPU()
	stats.Sys.Load = []float64{la.One, la.Five, la.Fifteen}

	if r.URL.Query().Get("mode") == "verbose" {
		stats.Runtime = util.GetRuntime().String()
		stats.Memory.HeapAlloc = humanize.Bytes(memStats.HeapAlloc)
		stats.Memory.HeapSys = humanize.Bytes(memStats.HeapSys)
		stats.Go.GCPause = gcStats.PauseTotal.String()
		stats.Go.GCLast = gcStats.LastGC.Format(time.RFC3339Nano)
		stats.Sys.Memory = humanize.Bytes(mem.Total)
		stats.Sys.Swap = humanize.Bytes(swap.Total)
		stats.Sys.Uptime = uptime.Format()
	} else {
		stats.Runtime = util.GetRuntime()
		stats.Memory.HeapAlloc = memStats.HeapAlloc
		stats.Memory.HeapSys = memStats.HeapSys
		stats.Go.GCPause = gcStats.PauseTotal
		stats.Go.GCLast = gcStats.LastGC
		stats.Sys.Memory = mem.Total
		stats.Sys.Swap = swap.Total
		stats.Sys.Uptime = uptime.Length
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(stats)
}
