package controller

import (
	"context"
	"errors"
	"net/http"
	"runtime/debug"

	"gitlab.com/bakyun/server/internal/services"
	"gitlab.com/bakyun/server/templates"

	"gitlab.com/bakyun/server/config"
)

// ErrorRequest returns an ErrorStruct with the specified fields
func ErrorRequest(cfg *config.Config, code int, err error, message ...string) *ErrorStruct {
	es := &ErrorStruct{
		Code:    code,
		Message: err.Error(),
		Error:   err,
		Stack:   config.Debug,
		JSON:    false,
		Config:  cfg,
	}

	if es.Stack {
		es.Trace = string(debug.Stack())
	}

	if len(message) > 0 {
		es.Message = message[0]
	}

	return es
}

// ErrorAPIRequest returns an ErrorStruct with the specified fields and JSON enabled output
func ErrorAPIRequest(cfg *config.Config, code int, err error, message ...string) *ErrorStruct {
	es := ErrorRequest(cfg, code, err, message...)
	es.JSON = true
	return es
}

// Error runs the error handler
func Error(w http.ResponseWriter, r *http.Request) {
	es, ok := r.Context().Value(errorValue).(*ErrorStruct)

	if !ok || es == nil {
		templates.Render(w, "error.jet", services.GetConfig(), &ErrorStruct{
			Code:  http.StatusInternalServerError,
			Error: errors.New("unkown error"),
			Stack: false,
			JSON:  false,
		})
		return
	}

	if es.JSON {
		println("TODO!!!")
	} else {
		templates.Render(w, "error.jet", es.Config, es)
	}
}

// CSRFError returns a HandlerFunc for CSRF errors
func CSRFError(c *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		Error(w, r.WithContext(context.WithValue(r.Context(), errorValue, ErrorRequest(c, http.StatusForbidden, errors.New("invalid csrf token"), "CSRF Forbidden"))))
	}
}
