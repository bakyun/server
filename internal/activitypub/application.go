//+build old

package activitypub

import (
	"context"
	"crypto"
	"github.com/go-pg/pg"
	"gitlab.com/bakyun/cache"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/model"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/vocab"
	"github.com/go-fed/httpsig"
	"go.uber.org/zap"
)

// Application defines the activitypub application
type Application struct {
	log    *zap.SugaredLogger
	cacher *cache.Cacher
	db     *pg.DB
	cfg    *config.Config
}

// Owns checks if an id is owned by this application
func (app *Application) Owns(_ context.Context, id *url.URL) bool {
	app.log.Debug("owns?",
		"left", id,
		"right", app.cfg.HTTP.Public,
	)
	return !id.IsAbs() || strings.HasPrefix(id.String(), app.cfg.HTTP.Public)

	//!TODO: check in db if id exists
}

// Get returns the public definition of a id
func (app *Application) Get(_ context.Context, id *url.URL, rw pub.RWType) (pub.PubObject, error) {
	app.log.Debug("get",
		"id", id,
		"rw", rw,
	)
	segments := strings.Split(id.Path, "/")

	// strip empty first result
	if segments[0] == "" {
		segments = segments[1:]
	}

	switch segments[0] {
	case "users":
		username := segments[1]

		rawModel, err := app.cacher.Fetch("model:account.name:"+username, func() (interface{}, error) {
			return model.AccountByName(app.db, username)
		})

		if err != nil {
			app.log.Warn("failed to query user!", "error", err)
			return nil, err
		}

		model, ok := rawModel.(*model.Account)
		if !ok {
			return nil, ErrUnkownObject
		}

		return model.Actor(app.cfg)

	case "uid":
		uid := segments[1]

		rawModel, err := app.cacher.Fetch("model:account.id:"+uid, func() (interface{}, error) {
			return model.AccountByID(app.db, uid)
		})

		if err != nil {
			app.log.Warn("failed to query user!", "error", err)
			return nil, err
		}

		model, ok := rawModel.(*model.Account)
		if !ok {
			return nil, ErrUnkownObject
		}

		return model.Actor(app.cfg)

	default:
		app.log.Debug("unkown resource to get")
		return nil, ErrUnkownObject
	}
}

func (app *Application) GetAsVerifiedUser(c context.Context, id *url.URL, authdUser *url.URL, rw pub.RWType) (pub.PubObject, error) {
	panic("not implemented")
}

func (app *Application) Has(c context.Context, id *url.URL) (bool, error) {
	panic("not implemented")
}

func (app *Application) Set(c context.Context, o pub.PubObject) error {
	panic("not implemented")
}

func (app *Application) GetInbox(c context.Context, r *http.Request, rw pub.RWType) (vocab.OrderedCollectionType, error) {
	panic("not implemented")
}

func (app *Application) GetOutbox(c context.Context, r *http.Request, rw pub.RWType) (vocab.OrderedCollectionType, error) {
	panic("not implemented")
}

func (app *Application) NewId(c context.Context, t pub.Typer) *url.URL {
	panic("not implemented")
}

func (app *Application) GetPublicKey(c context.Context, publicKeyId string) (pubKey crypto.PublicKey, algo httpsig.Algorithm, user *url.URL, err error) {
	panic("not implemented")
}

func (app *Application) CanAdd(c context.Context, o vocab.ObjectType, t vocab.ObjectType) bool {
	panic("not implemented")
}

func (app *Application) CanRemove(c context.Context, o vocab.ObjectType, t vocab.ObjectType) bool {
	panic("not implemented")
}

func (app *Application) OnFollow(c context.Context, s *streams.Follow) pub.FollowResponse {
	panic("not implemented")
}

func (app *Application) Unblocked(c context.Context, actorIRIs []*url.URL) error {
	panic("not implemented")
}

func (app *Application) FilterForwarding(c context.Context, activity vocab.ActivityType, iris []*url.URL) ([]*url.URL, error) {
	panic("not implemented")
}

func (app *Application) NewSigner() (httpsig.Signer, error) {
	panic("not implemented")
}

func (app *Application) PrivateKey(boxIRI *url.URL) (privKey crypto.PrivateKey, pubKeyId string, err error) {
	panic("not implemented")
}
