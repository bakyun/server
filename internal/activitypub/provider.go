package activitypub

import (
	"github.com/go-fed/activity/pub"
	"gitlab.com/bakyun/cache"
	"gitlab.com/bakyun/server/config"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
)

// Provider creates the activitypub service instance
func Provider(logger *zap.SugaredLogger, cfg *config.Config, cacher *cache.Cacher, pg *reform.DB) (pub.Actor, error) {
	actor := pub.NewFederatingActor(
		&CommonBehavior{
			db: pg,
		},
		new(FederatingProtocol),
		new(Database),
		new(clock),
	)

	return actor, nil
}
