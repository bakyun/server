package activitypub

import (
	"context"
	"net/http"
	"net/url"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams/vocab"
)

type FederatingProtocol struct{}

func (p *FederatingProtocol) AuthenticatePostInbox(c context.Context, w http.ResponseWriter, r *http.Request) (authenticated bool, err error) {
	panic("not implemented")
}

func (p *FederatingProtocol) Blocked(c context.Context, actorIRIs []*url.URL) (blocked bool, err error) {
	panic("not implemented")
}

func (p *FederatingProtocol) Callbacks(c context.Context) (wrapped pub.FederatingWrappedCallbacks, other []interface{}) {
	panic("not implemented")
}

func (p *FederatingProtocol) DefaultCallback(c context.Context, activity pub.Activity) error {
	panic("not implemented")
}

func (p *FederatingProtocol) MaxInboxForwardingRecursionDepth(c context.Context) int {
	panic("not implemented")
}

func (p *FederatingProtocol) MaxDeliveryRecursionDepth(c context.Context) int {
	panic("not implemented")
}

func (p *FederatingProtocol) FilterForwarding(c context.Context, potentialRecipients []*url.URL, a pub.Activity) (filteredRecipients []*url.URL, err error) {
	panic("not implemented")
}

func (p *FederatingProtocol) GetInbox(c context.Context, r *http.Request) (vocab.ActivityStreamsOrderedCollectionPage, error) {
	panic("not implemented")
}
