//+build old

package activitypub

import (
	"context"

	"github.com/go-fed/activity/streams"
)

type Callbacker struct {
}

func (c *Callbacker) Create(_ context.Context, s *streams.Create) error {
	panic("not implemented")
}

func (c *Callbacker) Update(_ context.Context, s *streams.Update) error {
	panic("not implemented")
}

func (c *Callbacker) Delete(_ context.Context, s *streams.Delete) error {
	panic("not implemented")
}

func (c *Callbacker) Add(_ context.Context, s *streams.Add) error {
	panic("not implemented")
}

func (c *Callbacker) Remove(_ context.Context, s *streams.Remove) error {
	panic("not implemented")
}

func (c *Callbacker) Like(_ context.Context, s *streams.Like) error {
	panic("not implemented")
}

func (c *Callbacker) Block(_ context.Context, s *streams.Block) error {
	panic("not implemented")
}

func (c *Callbacker) Follow(_ context.Context, s *streams.Follow) error {
	panic("not implemented")
}

func (c *Callbacker) Undo(_ context.Context, s *streams.Undo) error {
	panic("not implemented")
}

func (c *Callbacker) Accept(_ context.Context, s *streams.Accept) error {
	panic("not implemented")
}

func (c *Callbacker) Reject(_ context.Context, s *streams.Reject) error {
	panic("not implemented")
}
