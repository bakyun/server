package activitypub

import (
	"time"
)

type clock struct{}

func (*clock) Now() time.Time {
	return time.Now()
}
