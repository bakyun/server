package activitypub

import (
	"context"
	"net/url"

	"github.com/go-fed/activity/streams/vocab"
)

type Database struct{}

func (db *Database) Lock(c context.Context, id *url.URL) error {
	panic("not implemented")
}

func (db *Database) Unlock(c context.Context, id *url.URL) error {
	panic("not implemented")
}

func (db *Database) InboxContains(c context.Context, inbox *url.URL, id *url.URL) (contains bool, err error) {
	panic("not implemented")
}

func (db *Database) GetInbox(c context.Context, inboxIRI *url.URL) (inbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	panic("not implemented")
}

func (db *Database) SetInbox(c context.Context, inbox vocab.ActivityStreamsOrderedCollectionPage) error {
	panic("not implemented")
}

func (db *Database) Owns(c context.Context, id *url.URL) (owns bool, err error) {
	panic("not implemented")
}

func (db *Database) ActorForOutbox(c context.Context, outboxIRI *url.URL) (actorIRI *url.URL, err error) {
	panic("not implemented")
}

func (db *Database) ActorForInbox(c context.Context, inboxIRI *url.URL) (actorIRI *url.URL, err error) {
	panic("not implemented")
}

func (db *Database) Exists(c context.Context, id *url.URL) (exists bool, err error) {
	panic("not implemented")
}

func (db *Database) Get(c context.Context, id *url.URL) (value vocab.Type, err error) {
	panic("not implemented")
}

func (db *Database) Create(c context.Context, asType vocab.Type) error {
	panic("not implemented")
}

func (db *Database) Update(c context.Context, asType vocab.Type) error {
	panic("not implemented")
}

func (db *Database) Delete(c context.Context, id *url.URL) error {
	panic("not implemented")
}

func (db *Database) GetOutbox(c context.Context, inboxIRI *url.URL) (inbox vocab.ActivityStreamsOrderedCollectionPage, err error) {
	panic("not implemented")
}

func (db *Database) SetOutbox(c context.Context, inbox vocab.ActivityStreamsOrderedCollectionPage) error {
	panic("not implemented")
}

func (db *Database) NewId(c context.Context, t vocab.Type) (id *url.URL, err error) {
	panic("not implemented")
}

func (db *Database) Followers(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	panic("not implemented")
}

func (db *Database) Following(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	panic("not implemented")
}

func (db *Database) Liked(c context.Context, actorIRI *url.URL) (followers vocab.ActivityStreamsCollection, err error) {
	panic("not implemented")
}
