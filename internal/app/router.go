package app

import (
	"github.com/go-chi/chi"
	"gitlab.com/bakyun/nodeinfo"
)

// SetupRoutes for the router
func (a *Application) SetupRoutes(r chi.Router) {
	/*
		cfg := services.GetConfig()
		csrfmw := csrf.Protect(
			[]byte(cfg.HTTP.CSRF),
			csrf.CookieName("surf"),
			csrf.ErrorHandler(controller.CSRFError(cfg)),
			csrf.FieldName("address"), // try to honeypot the csrf token
			csrf.Secure(!config.Debug),
		)
	*/

	ni := a.container.NodeInfo()
	r.HandleFunc(nodeinfo.NodeInfoPath, ni.NodeInfoDiscover)
	r.HandleFunc(ni.Config.InfoURL, ni.NodeInfo)

	/*
		wf := wfc.GetWebfingerService()
		r.Handle(webfinger.WebFingerPath, wf)

		oc := controller.NewOAuthController(log.With("controller", "oauth"))
		r.Get(reverse.Add(routes.OAuthAuthorize, "/oauth/authorize"), oc.GetAuthorize)
		r.With(csrfmw).Post(reverse.Get(routes.OAuthAuthorize), oc.PostAuthorize)
		r.HandleFunc(reverse.Add(routes.OAuthAccess, "/oauth/token"), oc.HandleToken)

		if config.Debug {
			r.HandleFunc("/error", controller.Error)
			r.HandleFunc("/stats", controller.Stats)
		}
		r.Handle("/assets/*", http.StripPrefix("/assets", http.FileServer(templates.Assets)))
	*/
}
