package app

import (
	"gitlab.com/bakyun/server/services"
)

// Application struct
type Application struct {
	container *services.Container
}

// New Application
func New(container *services.Container) *Application {
	return &Application{
		container: container,
	}
}
