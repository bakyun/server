package services

import (
	"github.com/gopub/ioc"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/lib/util"
	"go.uber.org/zap"
)

var (
	loggerType      = (*zap.Logger)(nil)
	sugarLoggerType = (*zap.SugaredLogger)(nil)
	configType      = (*config.Config)(nil)
)

// RegisterLogger registers the given *zap.Logger instance
func RegisterLogger(l *zap.Logger) {
	ioc.RegisterValue(ioc.NameOf(loggerType), l)
	ioc.RegisterValue(ioc.NameOf(sugarLoggerType), l.Sugar())
	util.AddShutdownHook(l.Sync, true)
}

// RegisterConfig registers the given config struct
func RegisterConfig(c *config.Config) {
	ioc.RegisterValue(ioc.NameOf(configType), c)
}

// GetLogger returns the raw *zap.Logger instance
func GetLogger(fields ...zap.Field) *zap.Logger {
	l := ioc.Resolve(loggerType).(*zap.Logger)
	if len(fields) > 0 {
		l = l.With(fields...)
	}
	return l
}

// GetSugarLogger returns a wrapped *zasp.SugaredLogger
func GetSugarLogger(args ...interface{}) *zap.SugaredLogger {
	l := ioc.Resolve(sugarLoggerType).(*zap.SugaredLogger)
	if len(args) > 0 {
		l = l.With(args...)
	}
	return l
}

// GetConfig returns the config struct
func GetConfig() *config.Config {
	return ioc.Resolve(ioc.NameOf(configType)).(*config.Config)
}
