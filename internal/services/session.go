package services

import (
	"github.com/alexedwards/scs"
	"github.com/alexedwards/scs/stores/redisstore"
	"github.com/gopub/ioc"
)

var (
	scsType = (*scs.Manager)(nil)
)

// RegisterSession adds the *scs.Manager singleton
func RegisterSession() {
	ioc.RegisterSingletonCreator(ioc.NameOf(scsType), func(args ...interface{}) interface{} {
		r := GetRedis()
		return scs.NewManager(redisstore.New(r))
	})
}

// GetSession returns the *scs.Manager instance
func GetSession() *scs.Manager {
	return ioc.Resolve(scsType).(*scs.Manager)
}
