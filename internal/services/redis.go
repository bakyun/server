package services

import (
	"strings"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/gopub/ioc"
	"gitlab.com/bakyun/server/lib/util"
	"go.uber.org/zap"
)

var (
	redisType = (*redis.Pool)(nil)
)

const (
	redisDefaultPort          = ":6379"
	redisMaxIdleConnections   = 10
	redisMaxActiveConnections = 100
	redisMaxIdleTimeout       = 30 * time.Second
)

// RegisterRedis adds the *redis.Pool singleton
func RegisterRedis() {
	ioc.RegisterSingletonCreator(ioc.NameOf(redisType), func(args ...interface{}) interface{} {
		cfg := GetConfig()
		pool := new(redis.Pool)

		fullAddr := cfg.Redis.Host
		if !strings.Contains(fullAddr, ":") {
			fullAddr += redisDefaultPort
		}

		dialOpts := []redis.DialOption{
			redis.DialConnectTimeout(redisMaxIdleTimeout),
			redis.DialDatabase(cfg.Redis.Database),
		}

		if cfg.Redis.Password != "" {
			dialOpts = append(dialOpts, redis.DialPassword(cfg.Redis.Password))
		}

		pool.Dial = func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", fullAddr, dialOpts...)
			if err != nil {
				return nil, err
			}

			return redis.NewLoggingConn(conn, util.NewZapLogger(GetLogger(), zap.String("module", "redis")), ""), nil
		}

		pool.MaxIdle = redisMaxIdleConnections
		pool.MaxActive = redisMaxActiveConnections
		pool.IdleTimeout = redisMaxIdleTimeout
		pool.Wait = true

		util.AddShutdownHook(pool.Close)
		return pool
	})
}

// GetRedis returns the *redis.Pool instance
func GetRedis() *redis.Pool {
	return ioc.Resolve(redisType).(*redis.Pool)
}
