package services

import (
	"time"

	"github.com/gopub/ioc"
	"gitlab.com/bakyun/cache"

	// side effect: add postgres support to database/sql
	_ "github.com/jackc/pgx/stdlib"
)

var (
	cacheType = (*cache.Cacher)(nil)
)

// RegisterCache registers a new cacher instance
func RegisterCache() {
	ioc.RegisterSingletonCreator(ioc.NameOf(cacheType), func(args ...interface{}) interface{} {
		return cache.New(GetRedis(), 5*time.Minute, "cache:")
	})
}

// GetCache returns the registered cacher instance
func GetCache() *cache.Cacher {
	return ioc.Resolve(cacheType).(*cache.Cacher)
}
