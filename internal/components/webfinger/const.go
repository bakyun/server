package webfinger

// The different relations for webfinger
const (
	RelProfilePage  = "http://webfinger.net/rel/profile-page"
	TypeProfilePage = "text/html"

	RelAvatar = "http://webfinger.net/rel/avatar"

	RelSelf  = "self"
	TypeSelf = "application/activity+json"

	RelSubscribe = "http://ostatus.org/schema/1.0/subscribe"
)
