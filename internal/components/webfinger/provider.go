package webfinger

import (
	"github.com/gopub/ioc"
	"gitlab.com/bakyun/server/internal/services"
	webfinger "gitlab.com/bakyun/webfinger/server"
	"go.uber.org/zap"
)

var (
	typ = (*webfinger.Service)(nil)
)

func init() {
	ioc.RegisterSingletonCreator(ioc.NameOf(typ), func(args ...interface{}) interface{} {
		logger := services.GetSugarLogger("module", "webfinger")
		service, err := createWebfinger(logger)
		if err != nil {
			logger.DPanicw(
				"failed to create webfinger instance",
				"err", err,
			)
		}
		return service
	})
}

func createWebfinger(logger *zap.SugaredLogger) (*webfinger.Service, error) {
	cfg := services.GetConfig()
	cacher := services.GetCache()
	pg := services.GetReformDB()
	resolver, err := NewResolver(logger, cfg.HTTP.Public, cacher, pg, cfg)

	if err != nil {
		return nil, err
	}

	wf := webfinger.Default(resolver)
	// we probably work with a reverse proxy, which does the https magic
	wf.AllowHTTP = true

	return wf, nil
}

// GetWebfingerService returns the registered *webfinger.Service
func GetWebfingerService() *webfinger.Service {
	return ioc.Resolve(typ).(*webfinger.Service)
}
