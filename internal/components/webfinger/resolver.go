package webfinger

import (
	"database/sql"
	"net/url"
	"strings"

	"github.com/cking/reverse"
	"gitlab.com/bakyun/cache"
	"gitlab.com/bakyun/jrd"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/lib/util"
	"gitlab.com/bakyun/server/model"
	"gitlab.com/bakyun/server/routes"
	webfinger "gitlab.com/bakyun/webfinger/server"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
)

// Resolver resolves webfinger requests
type Resolver struct {
	host   *url.URL
	cache  *cache.Cacher
	db     *reform.DB
	log    *zap.SugaredLogger
	config *config.Config
}

// NewResolver creates a new webfinger resolver
func NewResolver(log *zap.SugaredLogger, host string, cacher *cache.Cacher, db *reform.DB, config *config.Config) (webfinger.Resolver, error) {
	parsedHost, err := url.Parse(host)
	if err != nil {
		return nil, err
	}

	resolver := &Resolver{
		host:   parsedHost,
		cache:  cacher,
		db:     db,
		log:    log,
		config: config,
	}

	return resolver, nil
}

// FindURI finds the resource for the given URI
func (r *Resolver) FindURI(uri *url.URL, rels []string) (*jrd.JRD, error) {
	r.log.Debugw("incoming finger uri request", "uri", uri)

	host := uri.Hostname()
	segments := strings.Split(uri.RawPath, "/")
	uname := segments[len(segments)-1]

	switch uri.RawPath {
	case reverse.Rev(routes.UserProfile, uname),
		reverse.Rev(routes.APUserProfile, uname):
		return r.FindUser(uname, host, rels)
	default:
		r.log.Debugw("unkown resource requested", "uri", uri)
		return nil, webfinger.ErrNotFound
	}
}

// FindUser finds the user given the username and hostname.
func (r *Resolver) FindUser(username, hostname string, _ []string) (res *jrd.JRD, err error) {
	r.log.Debugw("incoming finger acct request", "acct", username+"@"+hostname)

	if hostname != r.host.Host {
		return nil, webfinger.ErrNotFound
	}

	// query the database
	rawModel, err := r.cache.Fetch("model:"+username, func() (interface{}, error) {
		return model.AccountByName(r.db, username)
	})

	if err != nil {
		if err == sql.ErrNoRows {
			r.log.Debugw("no account found", "acct", username+"@"+hostname)
			return nil, webfinger.ErrNotFound
		}

		r.log.Warnw("failed to query user!", "err", err)
		return nil, err
	}

	model, ok := rawModel.(*model.Account)
	if !ok {
		r.log.Warnw("failed to parse model info", "acct", username+"@"+hostname)
		return nil, webfinger.ErrNotFound
	}

	var avatar string
	if model.Avatar != nil {
		if avaURL, err := util.ResolveVFSString(*model.Avatar, r.config); err != nil {
			r.log.Warnw("failed to resolve avatar url!", "error", err)
		} else {
			avatar = avaURL.String()
		}
	}

	// now build the resource
	res = new(jrd.JRD)

	res.Subject = "acct:" + username + "@" + hostname
	res.Aliases = []string{
		r.host.ResolveReference(&url.URL{Path: reverse.Rev(routes.UserProfile, username)}).String(),   // human readable end point
		r.host.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserProfile, username)}).String(), // machine readable (activity stream) endpoint
	}

	res.Links = []jrd.Link{
		{
			Rel:  RelProfilePage,
			Href: res.Aliases[0],
			Type: TypeProfilePage,
		},
		{
			Rel:  RelAvatar,
			Href: avatar,
			Type: *model.AvatarMIME,
		},
		{
			Rel:  RelSelf,
			Href: res.Aliases[1],
			Type: TypeSelf,
		},
		{
			Rel:      RelSubscribe,
			Template: r.host.ResolveReference(&url.URL{Path: "/subscribe", RawQuery: "account={uri}"}).String(),
		},
	}
	return res, nil
}
