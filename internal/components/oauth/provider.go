package oauth

import (
	"github.com/gopub/ioc"
	"github.com/openshift/osin"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/internal/services"
)

var (
	typ = (*osin.Server)(nil)
)

func init() {
	ioc.RegisterSingletonCreator(ioc.NameOf(typ), func(args ...interface{}) interface{} {
		return createOsinServer()
	})
}

func createOsinServer() *osin.Server {
	ocfg := osin.NewServerConfig()
	ocfg.AllowGetAccessRequest = true
	ocfg.AllowClientSecretInParams = true
	ocfg.RedirectUriSeparator = config.URISeparator

	storage := &Storage{
		db:      services.GetReformDB(),
		r:       services.GetRedis(),
		log:     services.GetSugarLogger("module", "oauth"),
		baseURL: services.GetConfig().HTTP.Public,
	}

	return osin.NewServer(ocfg, storage)
}

// GetServer returns the registered *osin.Server
func GetServer() *osin.Server {
	return ioc.Resolve(typ).(*osin.Server)
}
