package oauth

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/openshift/osin"
	"gitlab.com/bakyun/server/model"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
)

// Storage is for delegating oauth storage related objectives
type Storage struct {
	db      *reform.DB
	r       *redis.Pool
	log     *zap.SugaredLogger
	baseURL string
}

// Clone creates a copy of the storage adapter
func (s *Storage) Clone() osin.Storage {
	return s
}

// Close closes all connections of the adapter
func (s *Storage) Close() {
	// no-op
}

// GetClient fetches the client details (if existing)
func (s *Storage) GetClient(id string) (osin.Client, error) {
	app, err := model.ApplicationByID(s.db, id)
	if err != nil {
		s.log.Debugw("GetClient",
			"id", id,
			"err", err)
	}

	if err == sql.ErrNoRows {
		return nil, osin.ErrNotFound
	} else if err != nil {
		return nil, err
	} else {
		return app, nil
	}
}

// SetClient stores client settings in the storage
func (s *Storage) SetClient(id string, client osin.Client) error {
	app, ok := client.GetUserData().(*model.Application)
	if !ok {
		return ErrIncompatible
	}

	return s.db.Insert(app)
}

// SaveAuthorize saves the authorize codes
func (s *Storage) SaveAuthorize(data *osin.AuthorizeData) error {
	buf, err := json.Marshal(data)
	if err != nil {
		return err
	}

	conn := s.r.Get()
	defer conn.Close()

	_, err = conn.Do("SET", "oauth:authorize:"+data.Code, string(buf), "EX", data.ExpiresIn, "NX")
	return err
}

// LoadAuthorize loads the authorize data by the specified code
func (s *Storage) LoadAuthorize(code string) (*osin.AuthorizeData, error) {
	conn := s.r.Get()
	defer conn.Close()

	reply, err := conn.Do("GET", "oauth:authorize:"+code)
	if err == redis.ErrNil {
		return nil, osin.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	var buf []byte
	switch reply.(type) {
	case (string):
		buf = []byte(reply.(string))
	case ([]byte):
		buf = reply.([]byte)
	default:
		return nil, ErrIncompatible
	}

	data := new(osin.AuthorizeData)
	data.Client = new(model.Application)
	err = json.Unmarshal(buf, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// RemoveAuthorize removes the authorize data by the specified code
func (s *Storage) RemoveAuthorize(code string) error {
	conn := s.r.Get()
	defer conn.Close()

	conn.Do("DEL", "oauth:authorize:"+code)
	return nil
}

// SaveAccess saves access token data
func (s *Storage) SaveAccess(data *osin.AccessData) error {
	session := new(model.Session)
	session.ID = data.AccessToken
	session.ClientID = data.Client.GetId()
	session.RefreshToken = data.RefreshToken
	session.RedirectURI = data.RedirectUri
	session.ExpiresAt = data.ExpireAt()
	session.SetScopeString(data.Scope)
	if err := session.SetAuthorizeData(data.AuthorizeData); err != nil {
		return err
	}

	return s.db.Insert(session)
}

// LoadAccess loads the access data by token
func (s *Storage) LoadAccess(token string) (*osin.AccessData, error) {
	ad, err := s.loadAccessSimple(token, false)
	if err != nil {
		return nil, err
	}

	ad.AccessData, err = s.loadAccessSimple(token, true)
	if err != nil {
		return nil, err
	}

	return ad, nil
}

func (s *Storage) loadAccessSimple(token string, isRefresh bool) (*osin.AccessData, error) {
	var session *model.Session
	var err error

	if isRefresh {
		session, err = model.SessionByRefreshToken(s.db, token)
	} else {
		session, err = model.SessionByID(s.db, token)
	}

	if err != nil {
		return nil, err
	}

	ad := new(osin.AccessData)

	if !isRefresh {
		app, err := session.Client(s.db)
		if err != nil {
			return nil, err
		}
		ad.Client = app
	}

	ad.AccessToken = token
	ad.RefreshToken = session.RefreshToken
	ad.ExpiresIn = int32(session.ExpiresIn() / time.Second)
	ad.Scope = session.Scope()
	ad.RedirectUri = session.RedirectURI
	ad.CreatedAt = session.CreatedAt
	ad.UserData = session

	if !isRefresh {
		ad.AuthorizeData, err = session.AuthorizeData()
		if err != nil {
			return nil, err
		}

		ad.AuthorizeData.Client = ad.Client
	}
	/*
		// Authorize data, for authorization code
		AuthorizeData * AuthorizeData
	*/

	return ad, nil
}

// RemoveAccess removes an access data entry
func (s *Storage) RemoveAccess(token string) error {
	s.db.DeleteFrom(
		model.SessionTable,
		fmt.Sprintf("WHERE id = %s", s.db.Placeholder(1)),
		token,
	)
	return nil
}

// LoadRefresh loads access data by a refresh token
func (s *Storage) LoadRefresh(token string) (*osin.AccessData, error) {
	ad, err := s.loadAccessSimple(token, true)
	if err != nil {
		return nil, err
	}

	ad.AccessData, err = s.loadAccessSimple(token, true)
	if err != nil {
		return nil, err
	}

	ad.AuthorizeData, err = ad.UserData.(*model.Session).AuthorizeData()
	if err != nil {
		return nil, err
	}
	ad.AuthorizeData.Client, err = ad.UserData.(*model.Session).Client(s.db)
	if err != nil {
		return nil, err
	}

	return ad, nil
}

// RemoveRefresh removes an access data entry identified by the access token
func (s *Storage) RemoveRefresh(token string) error {
	s.db.DeleteFrom(
		model.SessionTable,
		fmt.Sprintf("WHERE refresh_token = %s", s.db.Placeholder(1)),
		token,
	)
	return nil
}
