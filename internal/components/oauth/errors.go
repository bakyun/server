package oauth

import "errors"

// All the errors
var (
	ErrIncompatible = errors.New("incompatible struct")
)
