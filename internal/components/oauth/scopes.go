package oauth

// Scopes is a list of various scopes and their descriptions
//
// This is based on mastodons scopes
var Scopes = map[string]string{
	"follow": "Modify your account relationships",
	"push":   "Receive push notifications",

	"read":                "Read all your data, including statuses, lists, blocks, mutes and more",
	"read:accounts":       "Read your accounts information",
	"read:blocks":         "See your blocks",
	"read:favourites":     "See your favorites",
	"read:filters":        "See your filters",
	"read:follows":        "See your follow list",
	"read:lists":          "See the lists you created",
	"read:mutes":          "See your mutes",
	"read:notifications":  "See your notifications",
	"read:reports":        "See your reports",
	"read:search":         "Allow searching",
	"read:statuses":       "Read all your statuses",
	"write":               "Write to your feed and modify your accounts",
	"write:accounts":      "Change your accounts",
	"write:blocks":        "Block accounts and domains",
	"write:favourites":    "Change favorites",
	"write:filters":       "Create filters",
	"write:follows":       "Follow people",
	"write:lists":         "Create custom lists",
	"write:media":         "Upload media files",
	"write:mutes":         "Mute people and/or conversations",
	"write:notifications": "Clear notifications",
	"write:reports":       "Report other people",
	"write:statuses":      "Publish new statuses",

	// TODO: more granularity for read/write:statuses
}
