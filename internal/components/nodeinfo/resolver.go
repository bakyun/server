package nodeinfo

import (
	"gitlab.com/bakyun/nodeinfo"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/model"
	"gopkg.in/reform.v1"
)

func NewResolver(cfg *config.Config, db *reform.DB) *Resolver {
	return &Resolver{
		config: cfg,
		db:     db,
	}
}

// Resolver resolves dynamic statistics
type Resolver struct {
	config *config.Config
	db     *reform.DB
}

// IsOpenRegistration for this node?
func (r Resolver) IsOpenRegistration() (bool, error) {
	// TODO: set registration via database
	return r.config.Instance.RegistrationEnabled, nil
}

// Usage statistics
func (r Resolver) Usage() (nodeinfo.Usage, error) {
	var u nodeinfo.Usage

	totalUsers, err := model.AccountCount(r.db, model.AccountActivityAll)
	if err != nil {
		return u, err
	}

	halfYearUsers, err := model.AccountCount(r.db, model.AccountActivityHalfYear)
	if err != nil {
		return u, err
	}

	monthUsers, err := model.AccountCount(r.db, model.AccountActivityMonth)
	if err != nil {
		return u, err
	}

	u = nodeinfo.Usage{
		Users: nodeinfo.UsageUsers{
			Total:          totalUsers,
			ActiveHalfYear: halfYearUsers,
			ActiveMonth:    monthUsers,
		},
	}

	return u, nil
}
