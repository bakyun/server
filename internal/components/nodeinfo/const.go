package nodeinfo

// The different cache types
const (
	CacheAllUsers      = "nodeinfo:users:all"
	CacheHalfYearUsers = "nodeinfo:users:6"
	CacheMonthUsers    = "nodeinfo:users:1"
)
