package nodeinfo

import "errors"

var (
	// ErrUnexpectedType is thrown when type conversion failed
	ErrUnexpectedType = errors.New("unexpected type")
)
