package services

import (
	"database/sql"
	"runtime"

	"github.com/jackc/pgx"

	"github.com/jackc/pgx/stdlib"
	"gitlab.com/bakyun/server/lib/zapx"
	"go.uber.org/zap"
	"gopkg.in/reform.v1"
	"gopkg.in/reform.v1/dialects/postgresql"
)

var (
	sqlDBType    = (*sql.DB)(nil)
	reformDBType = (*reform.DB)(nil)
)

// Postgres connection
func (c *Container) Postgres() *sql.DB {
	if c.postgres == nil {
		cfg := c.Configuration
		logger := c.Logger()
		xLogger := zapx.New(logger.With(zap.String("module", "pgx")))

		connConfig := cfg.Postgres.PGXConnConfig()
		connConfig.Logger = xLogger
		connConfig.LogLevel = pgx.LogLevelTrace
		c.postgres = stdlib.OpenDB(connConfig)

		runtime.SetFinalizer(c.postgres, func(pgx *sql.DB) {
			pgx.Close()
		})
	}

	return c.postgres
}

// PostgresReform initializes the reform adapter
func (c *Container) PostgresReform() *reform.DB {
	pgx := c.Postgres()
	return reform.NewDB(pgx, postgresql.Dialect, nil)
}
