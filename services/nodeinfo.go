package services

import (
	"gitlab.com/bakyun/nodeinfo"
	"gitlab.com/bakyun/server/config"
	bakyuninfo "gitlab.com/bakyun/server/internal/components/nodeinfo"
)

// NodeInfo container
func (c *Container) NodeInfo() *nodeinfo.Service {
	if c.nodeinfo == nil {
		cfg := c.Configuration
		db := c.PostgresReform()

		postFormats := []string{"text/plain"}
		if cfg.Instance.PostFormats.HTML {
			postFormats = append(postFormats, "text/html")
		}
		if cfg.Instance.PostFormats.Markdown {
			postFormats = append(postFormats, "text/markdown")
		}

		nicfg := nodeinfo.Config{
			BaseURL: cfg.HTTP.Public,
			InfoURL: "/.well-known/nodeinfo/2.0",
			Metadata: nodeinfo.Metadata{
				NodeName:                  cfg.Instance.Name,
				NodeDescription:           cfg.Instance.Description,
				Private:                   !cfg.Instance.Federate,
				AccountActivationRequired: cfg.Instance.ActivationRequired,
				Features:                  config.FeaturesSupported,
				/*
					TODO:
					StaffAccounts             []string     `json:"staffAccounts"`
				*/
				RestrictedNicknames: config.RestrictedNicknames,
				PostFormats:         postFormats,
				InvitesEnabled:      cfg.Instance.InvitesEnabled,
			},
			Protocols: []nodeinfo.NodeProtocol{
				nodeinfo.ProtocolActivityPub,
				nodeinfo.ProtocolOStatus,
			},
			Services: nodeinfo.Services{
				Inbound: []nodeinfo.NodeService{},
				Outbound: []nodeinfo.NodeService{
					nodeinfo.ServiceAtom,
					nodeinfo.ServiceRSS,
				},
			},
			Software: nodeinfo.SoftwareInfo{
				Name:    config.ApplicationName,
				Version: config.Version,
			},
		}

		c.nodeinfo = nodeinfo.NewService(nicfg, bakyuninfo.NewResolver(cfg, db))
	}

	return c.nodeinfo
}
