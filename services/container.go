package services

import (
	"database/sql"

	"github.com/go-chi/chi"
	"gitlab.com/bakyun/nodeinfo"
	"gitlab.com/bakyun/server/config"
	"go.uber.org/zap"
)

// Container for all dependencies
type Container struct {
	Configuration *config.Config

	logger   *zap.Logger
	router   chi.Router
	nodeinfo *nodeinfo.Service
	postgres *sql.DB
}

// Build the container
func Build(cfg *config.Config) *Container {
	c := new(Container)
	c.Configuration = cfg

	return c
}
