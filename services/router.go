package services

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/bakyun/server/lib/chix"
	"go.uber.org/zap"
)

// time in seconds for the max age of a preflight request
const corsMaxAge = 300

// Router instance
func (c *Container) Router() chi.Router {
	if c.router == nil {
		c.router = chi.NewRouter()
		c.router.Use(
			// catch panics
			middleware.Recoverer,

			// logging relevant data structures
			middleware.RequestID,
			middleware.RealIP,

			// logger
			middleware.RequestLogger(chix.NewZapFormatter(c.Logger().With(zap.String("module", "http")))),

			// security
			cors.New(cors.Options{
				AllowCredentials: false,
				AllowedMethods:   []string{"GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE", "PATCH"},
				AllowedOrigins:   []string{"*"}, // Federation must support all origins
				MaxAge:           corsMaxAge,
			}).Handler,

			// forward head if needed
			middleware.GetHead,
		)

		if c.Configuration.HTTP.Static != "" {
			staticAssets := c.Configuration.HTTP.Static
			c.router.Handle("/*", http.FileServer(http.Dir(staticAssets)))
		}
	}

	return c.router
}
