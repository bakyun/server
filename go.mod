module gitlab.com/bakyun/server

go 1.12

require (
	cloud.google.com/go v0.41.0 // indirect
	github.com/AlekSi/pointer v1.0.0 // indirect
	github.com/CloudyKit/fastprinter v0.0.0-20170127035650-74b38d55f37a // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible
	github.com/Restream/migration v0.0.0-20180522101823-a9f8cbbcb71b
	github.com/akyoto/color v1.8.6
	github.com/alexedwards/scs v1.4.1
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a
	github.com/aws/aws-sdk-go v1.20.16 // indirect
	github.com/c2fo/vfs v2.1.4+incompatible
	github.com/cking/reverse v0.0.0-20180922201656-c3c838ded33c
	github.com/cking/shutdown v0.1.2
	github.com/cloudfoundry/gosigar v1.1.0
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/creasty/defaults v1.3.0
	github.com/denisenkom/go-mssqldb v0.0.0-20190710001350-29e7b2419f38 // indirect
	github.com/dimes/dihedral v0.0.0-20190702034447-27313c956422
	github.com/dustin/go-humanize v1.0.0
	github.com/elgris/sqrl v0.0.0-20181124135704-90ecf730640a
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-fed/activity v0.4.0
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-playground/form v3.1.4+incompatible
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gomarkdown/markdown v0.0.0-20190222000725-ee6a7931a1e4
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/wire v0.3.0 // indirect
	github.com/gopub/ioc v1.0.0
	github.com/gopub/log v1.0.3
	github.com/gorilla/csrf v1.6.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.5.0+incompatible
	github.com/jinzhu/configor v1.1.0
	github.com/lib/pq v1.1.1
	github.com/magefile/mage v1.8.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/openshift/osin v1.0.1
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/rs/xid v1.2.1
	github.com/rs/zerolog v1.14.3
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sethvargo/go-diceware v0.0.0-20181024230814-74428ac65346
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/tsavola/pointer v1.0.0
	github.com/urfave/cli v1.20.0
	gitlab.com/bakyun/cache v0.0.0-20190215140532-6805c1fc3159
	gitlab.com/bakyun/jrd v0.0.0-20190215064341-153427b2ab83
	gitlab.com/bakyun/magician v0.0.0-20190210101925-cf313cf20e1e
	gitlab.com/bakyun/nodeinfo v0.0.0-20190219142841-77b45028de68
	gitlab.com/bakyun/ucrypt v0.0.0-20190320062620-e7136480353e
	gitlab.com/bakyun/webfinger v0.0.0-20190329161050-540110f81c9c
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190710143415-6ec70d6a5542 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
	golang.org/x/tools v0.0.0-20190710184609-286818132824 // indirect
	golang.org/x/xerrors v0.0.0-20190513163551-3ee3066db522
	gopkg.in/reform.v1 v1.3.3
	syreclabs.com/go/faker v1.1.0 // indirect
)
