package model

import (
	"crypto/rand"
	"encoding/hex"
	"strings"
	"time"

	"github.com/rs/xid"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/lib/util"

	"gopkg.in/reform.v1"
)

//go:generate go run gopkg.in/reform.v1/reform

// Application represents a row in application table.
//reform:application
type Application struct {
	ID          string    `reform:"id,pk"`
	Name        string    `reform:"name"`
	Secret      string    `reform:"secret"`
	RedirectURI *string   `reform:"redirect_uri"`
	Website     *string   `reform:"website"`
	Description *string   `reform:"description"`
	CreatedAt   time.Time `reform:"created_at"`
	UpdatedAt   time.Time `reform:"updated_at"`
	AllowOBB    bool      `reform:"allow_obb"`
	Icon        *string   `reform:"icon"`

	config *config.Config
}

// BeforeInsert hook
func (a *Application) BeforeInsert() error {
	if a.ID == "" {
		a.ID = xid.New().String()
	}

	if a.Secret == "" {
		b := make([]byte, 25)
		_, err := rand.Read(b)
		if err != nil {
			return err
		}

		a.Secret = hex.EncodeToString(b)
	}

	a.CreatedAt = time.Now()

	return nil
}

// BeforeUpdate hook
func (a *Application) BeforeUpdate() error {
	a.UpdatedAt = time.Now()
	return nil
}

// XID converts the base32 to a xid id
func (a *Application) XID() xid.ID {
	id, _ := xid.FromString(a.ID)
	return id
}

// ApplicationByID returns an application by id
func ApplicationByID(db *reform.DB, id string) (*Application, error) {
	a := new(Application)
	err := db.FindByPrimaryKeyTo(a, id)

	if err != nil {
		return nil, err
	}
	return a, nil
}

// Configure the model to have more metadata
func (a *Application) Configure(cfg *config.Config) *Application {
	a.config = cfg

	return a
}

// ResolveIcon returns the resolved icon url from the model
func (a *Application) ResolveIcon(defaultIcon ...string) string {
	defaultIco := ""
	if len(defaultIcon) > 0 {
		defaultIco = defaultIcon[0]
	}

	if a.config == nil || a.Icon == nil {
		return defaultIco
	}

	ava, err := util.ResolveVFSString(*a.Icon, a.config)
	if err != nil {
		return defaultIco
	}

	return ava.String()
}

// GetId returns the application id
func (a *Application) GetId() string { // revive:disable-line
	return a.ID
}

// GetSecret returns the application secret
func (a *Application) GetSecret() string {
	return a.Secret
}

// GetRedirectUri returns the the obb uri and if set the application's redirect uri
func (a *Application) GetRedirectUri() string { // revive:disable-line
	uris := make([]string, 0, 3)

	if a.AllowOBB {
		uris = append(uris, config.OBB)
	}

	if a.RedirectURI != nil {
		uris = append(uris, *a.RedirectURI)
	}

	return strings.Join(uris, config.URISeparator)
}

// GetUserData returns the whole application model
func (a *Application) GetUserData() interface{} {
	return a
}
