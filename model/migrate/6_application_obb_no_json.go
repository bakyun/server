package migrate

func init() {
	registerMigration("6_application_obb_no_json", q(
		(`ALTER TABLE application DROP COLUMN allow_obb_auto`),
	), q(
		(`ALTER TABLE application ADD COLUMN allow_obb_auto boolean NOT NULL DEFAULT false`),
	))
}
