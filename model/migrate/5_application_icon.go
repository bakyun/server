package migrate

func init() {
	registerMigration("5_application_icon", q(
		(`ALTER TABLE application ADD COLUMN icon character varying(100)`),
	), q(
		(`ALTER TABLE application DROP COLUMN icon`),
	))
}
