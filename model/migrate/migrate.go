package migrate

import (
	"database/sql"

	"github.com/Restream/migration"
)

type mf func(*sql.Tx) error

var migrations = make([]migration.Migration, 0)

func registerMigration(name string, up, down mf) {
	m := new(migration.Struct)
	m.NameString = name
	m.ApplyFunc = up
	m.RollbackFunc = down
	migrations = append(migrations, m)
}

func q(qs ...string) mf {
	return func(tx *sql.Tx) error {
		for _, q := range qs {
			_, err := tx.Exec(q)
			if err != nil {
				return err
			}
		}

		return nil
	}
}

// Migrate to the latest registered database layout
func Migrate(db *sql.DB) error {
	sch := migration.NewSchema(db, migration.DefaultSchemaName, migration.DefaultMigrationTableName)
	if err := sch.Init(); err != nil {
		return err
	}

	migs, err := sch.FindUnapplied(migrations)
	if err != nil {
		return err
	}

	_, err = sch.Apply(migs)
	return err
}
