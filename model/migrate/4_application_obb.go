package migrate

func init() {
	registerMigration("4_application_obb", q(
		(`ALTER TABLE application ADD COLUMN allow_obb boolean NOT NULL DEFAULT true`),
		(`ALTER TABLE application ADD COLUMN allow_obb_auto boolean NOT NULL DEFAULT false`),
	), q(
		(`ALTER TABLE application DROP COLUMN allow_obb`),
		(`ALTER TABLE application DROP COLUMN allow_obb_auto`),
	))
}
