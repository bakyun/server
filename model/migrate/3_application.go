package migrate

func init() {
	registerMigration("3_application", q(
		(`
CREATE TABLE application
(
    id character(20) NOT NULL,
    name character varying(100) NOT NULL,
    secret character(50) NOT NULL,
    redirect_uri character varying(100),
    website character varying(100),
    description text,
    scopes character varying(10)[],
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    updated_at timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
)
		`),
	), q(
		("DROP TABLE \"application\""),
	))
}
