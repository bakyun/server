package migrate

func init() {
	registerMigration("7_application_no_scope", q(
		(`ALTER TABLE application DROP COLUMN scopes`),
	), q(
		(`ALTER TABLE application ADD COLUMN scopes character varying(10)[]`),
	))
}
