package migrate

func init() {
	registerMigration("8_session", q(
		(`
CREATE TABLE session
(
    id character varying(100) NOT NULL,
    refresh_token character varying(100),
    redirect_uri character varying(250) NOT NULL,
    scopes character varying(25)[] NOT NULL,
    client_id character(20) NOT NULL,
    authorize_data json NOT NULL,
    expires_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    CONSTRAINT session_unique_refresh_token UNIQUE (refresh_token)
)
		`),
	), q(
		("DROP TABLE session"),
	))
}
