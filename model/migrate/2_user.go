package migrate

func init() {
	registerMigration("2_user", q(
		(`
CREATE TABLE "user"
(
    id character(20) NOT NULL,
    email character varying(250) NOT NULL,
    password character varying(200) NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT user_email UNIQUE (email)
)
		`),
		(`
ALTER TABLE account
    ADD CONSTRAINT account_owner FOREIGN KEY (owner_id)
    REFERENCES public."user" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
		`),
	), q(
		("ALTER TABLE account DROP CONSTRAINT account_owner"),
		("DROP TABLE \"user\""),
	))
}
