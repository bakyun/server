package migrate

func init() {
	registerMigration("1_initial", q(
		("CREATE TYPE account_type AS ENUM ('person', 'service', 'group', 'organization')"),
		(`
CREATE TABLE account
(
    id character(20) NOT NULL,
    name character varying(100) NOT NULL,
    display_name character varying(100),
    bio text,
    owner_id character(100),
    avatar character varying(100),
    avatar_mime character varying(100),
    banner character varying(100),
    banner_mime character varying(100),
    background character varying(100),
    background_mime character varying(100),
    location character varying(100),
    type account_type NOT NULL DEFAULT 'person'::account_type,
    last_activity timestamp without time zone NOT NULL DEFAULT now(),
    CONSTRAINT account_pkey PRIMARY KEY (id)
)
		`),
	), q(
		("DROP TABLE account"),
		("DROP TYPE account_type"),
	))
}
