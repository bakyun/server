package model

import (
	"fmt"

	"github.com/rs/xid"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/ucrypt"
	"gopkg.in/reform.v1"
)

//go:generate go run gopkg.in/reform.v1/reform

// User represents a row in user table.
//reform:user
type User struct {
	ID       string `reform:"id,pk"`
	Email    string `reform:"email"`
	Password string `reform:"password"`
}

// BeforeInsert hook
func (u *User) BeforeInsert() error {
	if u.ID == "" {
		u.ID = xid.New().String()
	}

	return nil
}

// XID converts the base32 to a xid id
func (u *User) XID() xid.ID {
	id, _ := xid.FromString(u.ID)
	return id
}

// Account returns the main account (type=person)
func (u *User) Account(db *reform.DB) (*Account, error) {
	a := new(Account)
	if err := db.SelectOneTo(
		u,
		fmt.Sprintf("owner_id = %s", db.Placeholder(1)),
		a.OwnerID,
	); err != nil {
		return nil, err
	}
	return a, nil
}

// UserByEmail searches the database for a user with the given email
func UserByEmail(db *reform.DB, id string) (*User, error) {
	u := new(User)
	err := db.SelectOneTo(
		u,
		fmt.Sprintf(
			"WHERE email = %s",
			db.Placeholder(1),
		),
		id,
	)

	if err != nil {
		return nil, err
	}
	return u, nil
}

// SetPassword updates the user password
func (u *User) SetPassword(cfg *config.Config, newPassword string) error {
	pw, err := ucrypt.HashPassword(cfg.UCrypt.Algorithm, []byte(newPassword), cfg.UCrypt.Configuration, 32)
	if err != nil {
		return err
	}

	u.Password = pw
	return nil
}

// VerifyPassword verifies if the given password matches the one in the database
func (u *User) VerifyPassword(pw string) bool {
	ok, err := ucrypt.VerifyPassword(u.Password, []byte(pw))
	if err != nil {
		ok = false
	}
	return ok
}
