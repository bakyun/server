package model

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/openshift/osin"

	"github.com/lib/pq"
	"gopkg.in/reform.v1"
)

//go:generate go run gopkg.in/reform.v1/reform

// Session represents a row in session table.
//reform:session
type Session struct {
	ID               string          `reform:"id,pk"`
	ClientID         string          `reform:"client_id"`
	RefreshToken     string          `reform:"refresh_token"`
	RedirectURI      string          `reform:"redirect_uri"`
	Scopes           pq.StringArray  `reform:"scopes"`
	RawAuthorizeData json.RawMessage `reform:"authorize_data"`
	CreatedAt        time.Time       `reform:"created_at"`
	ExpiresAt        time.Time       `reform:"expires_at"`
}

// BeforeInsert hook
func (s *Session) BeforeInsert() error {
	s.CreatedAt = time.Now()

	return nil
}

func SessionByID(db *reform.DB, id string) (*Session, error) {
	m := new(Session)
	err := db.FindByPrimaryKeyTo(m, id)

	if err != nil {
		return nil, err
	}
	return m, nil
}

func SessionByRefreshToken(db *reform.DB, token string) (*Session, error) {
	m := new(Session)
	err := db.SelectOneTo(
		m,
		fmt.Sprintf("WHERE refresh_token = %s", db.Placeholder(1)),
		token,
	)

	if err != nil {
		return nil, err
	}
	return m, nil
}

func (s *Session) Client(db *reform.DB) (*Application, error) {
	return ApplicationByID(db, s.ClientID)
}

func (s *Session) Scope() string {
	return strings.Join(s.Scopes, " ")
}

func (s *Session) SetScopeString(scopes string) {
	s.Scopes = strings.Split(scopes, " ")
}

func (s *Session) ExpiresIn(from ...time.Time) time.Duration {
	ts := time.Now()
	if len(from) > 0 {
		ts = from[0]
	}

	return s.ExpiresAt.Sub(ts)
}

func (s *Session) AuthorizeData() (*osin.AuthorizeData, error) {
	data := new(osin.AuthorizeData)
	if err := json.Unmarshal(s.RawAuthorizeData, data); err != nil {
		return nil, err
	}

	return data, nil
}

func (s *Session) SetAuthorizeData(data *osin.AuthorizeData) error {
	var ad *osin.AuthorizeData
	*ad = *data

	ad.Client = nil

	buf, err := json.Marshal(ad)
	if err != nil {
		return err
	}

	s.RawAuthorizeData = buf
	return nil
}
