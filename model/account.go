package model

import (
	"fmt"
	"time"

	"github.com/elgris/sqrl"
	"github.com/rs/xid"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/lib/util"
	"gopkg.in/reform.v1"
)

//go:generate go run gopkg.in/reform.v1/reform

// Account represents a row in account table.
//reform:account
type Account struct {
	ID             string      `reform:"id,pk"`
	Name           string      `reform:"name"`
	DisplayName    *string     `reform:"display_name"`
	Bio            *string     `reform:"bio"`
	OwnerID        string      `reform:"owner_id"`
	Avatar         *string     `reform:"avatar"`
	AvatarMIME     *string     `reform:"avatar_mime"`
	Location       *string     `reform:"location"`
	Type           AccountType `reform:"type"`
	Banner         *string     `reform:"banner"`
	BannerMIME     *string     `reform:"banner_mime"`
	Background     *string     `reform:"background"`
	BackgroundMIME *string     `reform:"background_mime"`
	LastActivity   time.Time   `reform:"last_activity"`

	config *config.Config
}

// BeforeInsert hook
func (a *Account) BeforeInsert() error {
	if a.ID == "" {
		a.ID = xid.New().String()
	}

	if a.Type == "" {
		a.Type = AccountTypePerson
	}

	return nil
}

// XID converts the base32 to a xid id
func (a *Account) XID() xid.ID {
	id, _ := xid.FromString(a.ID)
	return id
}

// Owner returns the owner
func (a *Account) Owner(db *reform.DB) (*User, error) {
	u := new(User)
	if err := db.FindByPrimaryKeyTo(u, a.OwnerID); err != nil {
		return nil, err
	}
	return u, nil
}

// AccountType is the account_type enum
type AccountType string

// The different account types
const (
	AccountTypePerson       AccountType = "person"
	AccountTypeService                  = "service"
	AccountTypeGroup                    = "group"
	AccountTypeOrganization             = "organization"
)

// AccountActivity specifies the different activity types
type AccountActivity int

// The different activity types
const (
	AccountActivityAll AccountActivity = iota
	AccountActivityHalfYear
	AccountActivityMonth
)

// AccountByName searches the account table for the account by the given name
func AccountByName(db *reform.DB, name string) (*Account, error) {
	a := new(Account)
	err := db.SelectOneTo(
		a,
		fmt.Sprintf(
			"WHERE name = %s",
			db.Placeholder(1),
		),
		name,
	)

	if err != nil {
		return nil, err
	}
	return a, nil
}

// AccountForUser returns the main account (type = person) for the specified user id
func AccountForUser(db *reform.DB, uid string) (*Account, error) {
	a := new(Account)
	err := db.SelectOneTo(
		a,
		fmt.Sprintf(
			"WHERE type = %s AND owner_id = %s",
			db.Placeholder(1),
			db.Placeholder(2),
		),
		AccountTypePerson, uid,
	)

	if err != nil {
		return nil, err
	}
	return a, nil
}

// AccountsForUser returns the list of accounts for a specific user id
func AccountsForUser(db *reform.DB, uid string, cfg *config.Config) ([]*Account, error) {
	structs, err := db.SelectAllFrom(
		AccountTable,
		fmt.Sprintf(
			"WHERE owner_id = %s ORDER BY type",
			db.Placeholder(1),
		),
		uid,
	)

	if err != nil {
		return nil, err
	}

	data := make([]*Account, len(structs))
	for i, str := range structs {
		data[i] = str.(*Account)
		data[i].Configure(cfg)
	}

	return data, nil
}

// AccountCount returns the number of active accounts in the specified amount of time
func AccountCount(db *reform.DB, period AccountActivity) (int, error) {
	q := sqrl.Select("COUNT(*)").From(AccountTable.Name())

	now := time.Now().UTC()
	switch period {
	case AccountActivityHalfYear:
		q.Where(
			fmt.Sprintf("last_activity >= %s", db.Placeholder(1)),
			now.AddDate(0, -6, 0),
		)
	case AccountActivityMonth:
		q.Where(
			fmt.Sprintf("last_activity >= %s", db.Placeholder(1)),
			now.AddDate(0, -1, 0),
		)
	}

	sql, args, err := q.ToSql()
	if err != nil {
		return 0, err
	}

	var count int
	err = db.QueryRow(sql, args...).Scan(&count)
	if err != nil {
		db.Logger.After(sql, args, time.Duration(0), err)
	}
	return count, err
}

// Configure the model to have more metadata
func (a *Account) Configure(cfg *config.Config) *Account {
	a.config = cfg

	return a
}

// ResolveAvatar returns the resolved avatar url from the model
func (a *Account) ResolveAvatar(defaultAvatar ...string) string {
	defaultAva := ""
	if len(defaultAvatar) > 0 {
		defaultAva = defaultAvatar[0]
	}

	if a.config == nil || a.Avatar == nil {
		return defaultAva
	}

	ava, err := util.ResolveVFSString(*a.Avatar, a.config)
	if err != nil {
		return defaultAva
	}

	return ava.String()
}

// GetName tries to use .DisplayName if available and falls back to .Name
func (a *Account) GetName() string {
	if a.DisplayName == nil || *a.DisplayName == "" {
		return a.Name
	}

	return *a.DisplayName
}

/*
// Actor converts the current account model into a AS actor
func (a *Account) Actor(cfg *config.Config) (actor vocab.ObjectType, err error) {
	baseURL, err := url.Parse(cfg.HTTP.Public)
	if err != nil {
		return nil, err
	}

	switch a.Type {
	case AccountTypePerson:
		actor = new(vocab.Person)
	default:
		return nil, errors.New("not implemented")
	}

	actor.SetId(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserProfile, a.Name)}))
	actor.SetPreferredUsername(a.Name)

	displayName := a.Name
	if a.DisplayName != "" {
		displayName = a.DisplayName
	}
	actor.AppendNameString(displayName)
	actor.AppendSummaryString(a.Bio)

	if a.Location != "" {
		loc := &vocab.Place{}
		loc.AppendNameString(a.Location)
		// FIXME: lat/long/alt ? => https://www.w3.org/TR/activitystreams-vocabulary/#dfn-location
		actor.AppendLocationObject(loc)
	}

	actor.AppendUrlAnyURI(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.UserProfile, a.Name)}))

	if a.Avatar != "" {
		img := &vocab.Image{}
		pub, err := util.ResolveVFSString(a.Avatar, cfg)
		if err != nil {
			return nil, err
		}

		img.AppendUrlAnyURI(pub)
		actor.AppendIconImage(img)
	}

	if a.Banner != "" {
		img := &vocab.Image{}
		pub, err := util.ResolveVFSString(a.Banner, cfg)
		if err != nil {
			return nil, err
		}
		img.AppendUrlAnyURI(pub)
		actor.AppendImageImage(img)
	}

	// following
	// followers
	// liked
	// streams
	// endpoints
	//  oauthAuthorizationEndpoint
	//  oauthTokenEndpoint
	//  signClientKey
	//  sharedInbox
	// publicKey
	// isCat

	// tag <= holds custom emojis used

	actor.SetInboxAnyURI(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserInbox, a.Name)}))
	actor.SetOutboxAnyURI(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserOutbox, a.Name)}))
	actor.SetFollowersAnyURI(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserFollower, a.Name)}))
	actor.SetFollowingAnyURI(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APUserFollowing, a.Name)}))

	endpoints := &vocab.Object{}
	// endpoints.SetOauthAuthorizationEndpoint(util.MustURLParse(baseURL + "/oauth/authorize"))
	// endpoints.SetOauthTokenEndpoint(util.MustURLParse(baseURL + "/oauth/token"))
	endpoints.SetSharedInbox(baseURL.ResolveReference(&url.URL{Path: reverse.Rev(routes.APInbox)}))
	actor.SetEndpoints(endpoints)

	// web security
	//actor = ap.AddWebSecurityContext(a.PublicKey, actor)

	return actor, nil
}*/
