package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"gitlab.com/bakyun/server/internal/app"

	"go.uber.org/zap"

	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/services"
)

var (
	configFile = flag.String("config", "", "Specify custom config file to load")
	version    = flag.Bool("version", false, "Display version information")
)

func main() {
	flag.Parse()
	if *version {
		fmt.Printf(
			"%v%v\n  -- %v %v/%v\n",
			config.ApplicationName, config.Version,
			runtime.Version(), runtime.GOOS, runtime.GOARCH,
		)

		return
	}
	os.Exit(run())
}

func run() (exit int) {
	cfg := new(config.Config)
	cfg.Load(*configFile)
	container := services.Build(cfg)
	logger := container.Logger()
	compiled, _ := time.Parse(time.RFC3339, config.BuildDate)

	logger.Info(
		config.ApplicationName+" starting up",
		zap.Time("compiled", compiled),
		zap.String("version", config.LongVersion),
	)

	router := container.Router()
	s := &http.Server{
		Addr:    cfg.HTTP.Listen,
		Handler: router,
	}

	app := app.New(container)
	app.SetupRoutes(router)

	errChan := make(chan error, 1)
	stopChan := make(chan os.Signal, 1)

	go func(h *http.Server, cfg *config.Config, errChan chan error) {
		var err error

		if cfg.HTTP.SSL.Cert != "" && cfg.HTTP.SSL.Key != "" {
			err = h.ListenAndServeTLS(cfg.HTTP.SSL.Cert, cfg.HTTP.SSL.Key)
		} else {
			err = h.ListenAndServe()
		}

		errChan <- err
	}(s, cfg, errChan)
	defer s.Close()

	signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	logger.Info(
		"server started",
		zap.String("addr", cfg.HTTP.Listen),
	)

	select {
	case err := <-errChan:
		if err != nil {
			logger.Fatal(
				"server returned with an error",
				zap.Error(err),
			)
		}
	case sig := <-stopChan:
		logger.Debug(
			"received stop signal",
			zap.Stringer("signal", sig),
		)
	}

	return 0
}
