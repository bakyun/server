package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/bakyun/server/config"
	"gitlab.com/bakyun/server/services"
)

var container *services.Container

func main() {
	app := cli.NewApp()

	if config.GitState == "dirty" {
		app.Version = config.LongVersion
	} else {
		app.Version = config.Version
	}

	app.Commands = []cli.Command{
		{
			Name:        "database",
			Aliases:     []string{"db"},
			Usage:       "Database related operations",
			Subcommands: dbCommands,
		},
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config",
			Usage: "Set the path of the configuration file",
			Value: "",
		},
	}

	app.Before = func(c *cli.Context) error {
		configFile := c.String("config")
		cfg := new(config.Config)
		cfg.Load(configFile)
		container = services.Build(cfg)

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
