package main

import (
	"github.com/urfave/cli"
	"gitlab.com/bakyun/server/model/migrate"
	"go.uber.org/zap"
)

var dbCommands = []cli.Command{
	{
		Name:    "migrate",
		Aliases: []string{"m"},
		Usage:   "Migrate database to the newest version",
		Action: func(c *cli.Context) error {
			l := container.Logger()
			l.Info("Starting migration...")

			err := migrate.Migrate(container.Postgres())
			if err != nil {
				l.Error("Migration failed!", zap.Error(err))
			} else {
				l.Info("Migration done!")
			}

			return err
		},
	},
}
