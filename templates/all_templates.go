package templates

import (
	"io"
	"regexp"
	"strconv"
	"strings"

	"github.com/CloudyKit/jet"
	"gitlab.com/bakyun/server/config"
)

var (
	set = jet.NewHTMLSetLoader(nil)

	reCompileError = regexp.MustCompile(`^template: ([^:]+):(\d+): (.*)$`)
)

func addTemplate(name, content string) {
	_, err := set.LoadTemplate(name, content)
	if err != nil {
		matches := reCompileError.FindStringSubmatch(err.Error())
		if len(matches) == 3 {
			lno, _ := strconv.Atoi(matches[1])

			lines := strings.Split(content, "\n")
			println(strings.Join(lines[lno-5:lno+5], "\n"))
			panic(err)
		} else {
			panic(err)
		}
	}
}

// Render the specified template to wr using the given data context
func Render(wr io.Writer, template string, cfg *config.Config, data interface{}) error {
	tpl, err := set.GetTemplate(template)
	if err != nil {
		return err
	}

	vm := make(jet.VarMap)
	vm.Set("config", cfg)

	return tpl.Execute(wr, vm, data)
}
