package templates

import (
	"net/http"
	"reflect"

	"github.com/CloudyKit/jet"
)

func init() {
	set.AddGlobalFunc("httpstatus", func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("httpstatus", 1, 1)

		code := a.Get(0).Int()

		return reflect.ValueOf(http.StatusText(int(code)))
	})

	addTemplate("error.jet", `
		{{ extends "box-layout.jet" }}
		{{ block title() }}{{ config.Instance.Name }} » {{ .Code }} {{ .Code | httpstatus }}{{ end }}

		{{ block boxHeader() }}
			<video autoplay loop src="/assets/bakyun.webm" class="no-border"></video>
		{{ end }}

		{{ block boxMeta() }}
			<h1 class="error">{{ .Code }} <span>- {{ .Code | httpstatus }}</span></h1>
		{{ end }}

		{{ block boxBody() }}
			<p>{{ .Message }}</p>

			{{ if .Stack }}
				<pre class="pre-line">
{{ .Error.Error() }}

{{ .Trace }}
				</pre>
			{{ end }}
		{{ end }}
	`)
}
