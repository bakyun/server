// +build build

package main

import (
	"net/http"

	"github.com/shurcooL/vfsgen"
)

func main() {
	assets := http.FileSystem(http.Dir("./assets"))
	vfsgen.Generate(assets, vfsgen.Options{
		Filename:     "assets.go",
		PackageName:  "templates",
		BuildTags:    "",
		VariableName: "Assets",
	})
}
