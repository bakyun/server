Array.from(document.querySelectorAll('[data-copy]')).forEach(el => {
  copyEl = document.createElement('span')
  copyEl.classList.add('entypo-clipboard')
  copyEl.addEventListener('click', () => {
    el.select()
    document.execCommand('copy')
  })

  el.parentElement.insertBefore(copyEl, el.nextSibling)
})
