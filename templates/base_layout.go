package templates

func init() {
	addTemplate("layout.jet", `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>{{ yield title() }}</title>
    <link rel="stylesheet" href="/assets/normalize.css" />
    <link rel="stylesheet" href="/assets/entypo.css" />
    <link rel="stylesheet" href="/assets/ptsans.css" />
		<link rel="stylesheet" href="/assets/style.css" />
		<script src="/assets/script.js" defer></script>
  </head>
  <body>
    {{ yield body() }}
  </body>
</html>
	`)

	addTemplate("box-layout.jet", `
		{{ extends "layout.jet" }}
		{{ block boxHeader() }}
			<h1>{{ yield boxTitle() }}</h1>
		{{ end }}
		{{ block body() }}
			<div class="box">
				<div class="box-header">
					{{ yield boxHeader() }}
	      </div>

				<div class="box-meta">
					{{ yield boxMeta() }}
	      </div>

				<div class="box-body">
					{{ yield boxBody() }}
	      </div>
	    </div>
		{{ end }}
	`)
}
