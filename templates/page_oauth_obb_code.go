package templates

func init() {
	addTemplate("oauth_obb_code.jet", `
		{{ extends "box-layout.jet" }}
		{{ block title() }}{{ config.Instance.Name }} » Authorization Code for {{ .App.Name }}{{ end }}

		{{ block boxHeader() }}
			<video autoplay loop src="/assets/bakyun.webm" class="no-border"></video>
		{{ end }}

		{{ block boxMeta() }}{{ end }}

		{{ block boxBody() }}
			<p>
				This is your authorization code:
			</p>

			<input value="{{ .Code }}" data-copy />

			<p>
				Please copy and paste it into your application now.
			</p>
		{{ end }}
	`)
}
