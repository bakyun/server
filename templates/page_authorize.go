package templates

import (
	"reflect"

	"github.com/CloudyKit/jet"
	"gitlab.com/bakyun/server/internal/components/oauth"
)

func init() {
	set.AddGlobalFunc("scope_description", func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("scope_description", 1, 1)
		scope := a.Get(0).String()

		desc, ok := oauth.Scopes[scope]

		if !ok {
			return reflect.ValueOf("Unkown permission, take care!")
		}

		return reflect.ValueOf(desc)
	})

	addTemplate("authorize.jet", `
		{{ extends "box-layout.jet" }}
		{{ import "_form.jet" }}
		{{ block title() }}{{ config.Instance.Name }} » Authorize {{ .App.Name }}{{ end }}

		{{ block boxTitle() }}Authorize {{ .App.Name }} to access your personal data{{ end }}

		{{ block boxMeta() }}
			<div class="net">
          <div class="net-node">
            <img src="{{ .Account.ResolveAvatar("/assets/default_avatar.png") }}" alt="Avatar of {{ .Account.GetName() }}" class="node-icon" />
            <div class="node-name">
              {{ .Account.GetName() }}
            </div>
          </div>
          <div class="net-node">
            <img src="{{ .App.ResolveIcon("/assets/default_application.png") }}" alt="Icon of {{ .App.Name }}" class="node-icon" />
            <div class="node-name">
              {{ .App.Name }}
            </div>
          </div>
				</div>

				{{ if ! isnil(.App.Description) }}
					<p>{{ .App.Description | p | md_simple | raw }}</p>
				{{ end }}
		{{ end }}

		{{ block boxBody() }}
			<form class="form" method="POST">
				{{ if .Errors.HasErrors("") }}
					{{ range e := .Errors[""] }}
						{{ e }}
					{{ end }}
				{{ end }}

				<div>
					<h2>Default Account</h2>
					<div class="description">
						This is for Mastodon, Pleroma and AP Social client support
					</div>
					<select name="default_account">
						{{ range a := .Accounts }}
							<option value="{{ a.ID }}">
								{{ a.GetName() }}
							</option>
						{{ end }}
					</select>
				</div>

				<div class="permissions">
					<h2>Permissions </h2>
					<ul>
						{{ range scope := .Scopes }}
							<li>
								<h3>{{ scope }}</h3>
								<p>{{ scope | scope_description }}</p>
							</li>
						{{ end }}
					</ul>
				</div>

				{{ .CSRF | raw }}

				<div class="form-row form-buttons">
					<button type="submit" class="confirm" name="ok" value="1">
						Allow
					</button>
					<button type="submit" class="cancel" name="ok" value="0">
						Deny
					</button>
				</div>
			</form>
		{{ end }}
	`)
}
