package templates

import (
	"reflect"

	"github.com/gomarkdown/markdown/html"

	"github.com/gomarkdown/markdown"

	"github.com/gomarkdown/markdown/parser"

	"github.com/CloudyKit/jet"
)

func init() {
	set.AddGlobalFunc("isnil", func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("isnil", 1, 1)
		arg := a.Get(0)

		return reflect.ValueOf(arg.IsNil())
	})

	set.AddGlobalFunc("p", func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("p", 1, 1)
		arg := a.Get(0)

		if arg.IsNil() {
			return arg
		}

		return reflect.Indirect(arg)
	})

	set.AddGlobalFunc("md_simple", func(a jet.Arguments) reflect.Value {
		a.RequireNumOfArguments("md_simple", 1, 1)
		md := a.Get(0).String()
		buff := markdown.ToHTML([]byte(md), parser.NewWithExtensions(
			parser.NoIntraEmphasis|
				parser.Autolink|
				parser.Strikethrough|
				parser.HardLineBreak|
				parser.NoEmptyLineBeforeBlock|
				parser.BackslashLineBreak|
				parser.SuperSubscript,
		), html.NewRenderer(html.RendererOptions{
			Flags: html.SkipHTML |
				html.SkipImages |
				html.Safelink |
				html.NofollowLinks |
				html.NoreferrerLinks |
				html.HrefTargetBlank |
				html.CommonFlags,
		}))

		return reflect.ValueOf(string(buff))
	})
}
