package templates

func init() {
	addTemplate("_form.jet", `
		{{ block formInput(label, id, type="text", name="", required=false, placeholder="", icon="") }}
			<div class="form-row">
				<label for="{{ id }}" {{ if required }}class="required"{{ end }}>
					{{ label }}
				</label>

				{{ if len(icon) > 0 }}
				<div class="form-input-wrapper">
					<span class="entypo-{{icon}} form-prefix"></span>
				{{ end }}
					<input
						type="{{ type }}"
						{{ if len(name) > 0 }}
						name="{{ name }}"
						{{ else }}
						name="{{ id }}"
						{{ end }}
						id="{{ id }}"
						placeholder="{{placeholder}}"
						{{ if required }}
						required
						{{ end }}
					/>
				{{ if len(icon) > 0 }}
				</div>
				{{ end }}

				{{ yield content }}
			</div>
		{{ end }}
	`)
}
