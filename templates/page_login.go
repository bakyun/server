package templates

func init() {
	addTemplate("login.jet", `
		{{ extends "box-layout.jet" }}
		{{ import "_form.jet" }}
		{{ block title() }}{{ config.Instance.Name }} » Login{{ end }}

		{{ block boxTitle() }}Login{{ end }}

		{{ block boxMeta() }}
			<nav class="tabs">
				<span class="active">
					Login
				</span>
				<span class="">
					<a href="#register">Register</a>
				</span>
			</nav>
		{{ end }}

		{{ block boxBody() }}
			<form class="form" method="POST">
				{{ if .Errors.HasErrors("") }}
					{{ range e := .Errors[""] }}
						{{ e }}
					{{ end }}
				{{ end }}

				{{ yield formInput(label="Username", id="username", icon="user", required=true, placeholder="user@domain") content }}
					{{ if .Errors.HasErrors("username") }}
						{{ range e := .Errors["username"] }}
							{{ e }}
						{{ end }}
					{{ end }}
				{{ end }}
				{{ yield formInput(label="Password", id="password", icon="key", required=true, placeholder="s3Krit", type="password") content }}
					{{ if .Errors.HasErrors("password") }}
						{{ range e := .Errors["password"] }}
							{{ e }}
						{{ end }}
					{{ end }}
				{{ end }}

				{{ .CSRF | raw }}

				<div class="form-row form-buttons">
					<button type="submit" class="confirm" name="ok" value="1">
						Login
					</button>
					<button type="submit" class="cancel" name="ok" value="0">
						Cancel
					</button>
				</div>
			</form>
		{{ end }}
	`)
}
