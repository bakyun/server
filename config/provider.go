package config

import (
	"github.com/dimes/dihedral/embeds"
)

// Provider module for configuration
type Provider struct {
	provided embeds.ProvidedModule

	Config *Config
}

// ProvidesConfig provides the whole configuration object
func (p *Provider) ProvidesConfig() *Config {
	return p.Config
}

// ProvidesLoggingConfig method
func (p *Provider) ProvidesLoggingConfig() []Logging {
	return p.Config.Logging
}
