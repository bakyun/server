package config

// Instance configuration
type Instance struct {
	Name                string `yaml:"name" default:"Yet another Bakyun instance"`
	Description         string `yaml:"description" default:"A bakyun powered federated server"`
	RegistrationEnabled bool   `yaml:"registration-enabled"`
	Federate            bool   `yaml:"federate"`
	ActivationRequired  bool   `yaml:"activation-required"`
	InvitesEnabled      bool   `yaml:"invites-enabled"`
	PostFormats         struct {
		HTML     bool `yaml:"html"`
		Markdown bool `yaml:"markdown"`
	} `yaml:"post-formats"`
}
