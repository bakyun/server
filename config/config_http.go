package config

// HTTP config
type HTTP struct {
	Listen string `default:"localhost:8848" yaml:"listen"`
	Public string `default:"http://localhost:8848" yaml:"public"`
	Static string `yaml:"static"`

	SSL struct {
		Cert string `yaml:"cert"`
		Key  string `yaml:"key"`
	} `yaml:"ssl"`

	CSRF string `default:"12345678901234567890123456789012" yaml:"csrf"`
}
