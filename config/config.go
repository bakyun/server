package config

import (
	"github.com/jinzhu/configor"
)

// Config parses the config file
type Config struct {
	Instance Instance  `yaml:"instance"`
	Logging  []Logging `yaml:"logging"`
	Postgres Postgres  `yaml:"postgres"`
	HTTP     HTTP      `yaml:"http"`

	Redis struct {
		Host     string `yaml:"host"`
		Password string `yaml:"password"`
		Database int    `yaml:"database"`
	} `yaml:"redis"`

	Store struct {
		FileSystem struct {
			Path       string `yaml:"path" default:"public/uploads"`
			PublicPath string `yaml:"public-path"`
		} `yaml:"fs"`

		S3 struct {
			Endpoint     string `yaml:"endpoint"`
			Region       string `yaml:"region"`
			Bucket       string `yaml:"bucket"`
			BasePath     string `yaml:"base-path"`
			AccessKey    string `yaml:"access-key"`
			AccessSecret string `yaml:"access-secret"`
			PathStyle    bool   `yaml:"path-style"`
			PublicPath   string `yaml:"public-path"`
		} `yaml:"s3"`
		DefaultProvider string `yaml:"default-provider" default:"fs"`
	} `yaml:"store"`

	ActivityPub ActivityPub `yaml:"activity-pub"`

	UCrypt struct {
		Algorithm     string `default:"argon2" yaml:"algorithm"`
		Configuration string `default:"id,16,16384" yaml:"configuration"`
	} `flag:"-" yaml:"ucrypt"`

	/*
		Cookie Cookie `yaml:"cookie" flag:"-"`

		ActivityPub ActivityPub `flag:"-" yaml:"ap"`
	*/
}

// Load the configuration
func (c *Config) Load(file string) error {
	return configor.New(&configor.Config{
		Debug:                false,
		Verbose:              false,
		Silent:               true,
		ErrorOnUnmatchedKeys: false,
		ENVPrefix:            ApplicationName,
	}).Load(
		c,
		"config.yml",
		"config.yaml",
		ApplicationName+".yml",
		ApplicationName+".yaml",
		file,
	)
}
