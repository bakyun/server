package config

import (
	"strconv"
	"strings"

	"github.com/jackc/pgx"
)

// Postgres config
type Postgres struct {
	Host     string `yaml:"host"`
	Database string `yaml:"database"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

// PGXConnConfig conversion
func (c *Postgres) PGXConnConfig() pgx.ConnConfig {
	hostSplit := strings.SplitN(c.Host, ":", 2)
	var port uint16 = 5432
	if len(hostSplit) == 2 {
		p, err := strconv.ParseUint(hostSplit[1], 10, 16)
		if err != nil {
			panic(err)
		}
		port = uint16(p)
	}

	return pgx.ConnConfig{
		Host:     hostSplit[0],
		Port:     port,
		Database: c.Database,
		User:     c.User,
		Password: c.Password,
	}
}
