package config

import (
	"time"

	"golang.org/x/time/rate"
)

// ActivityPub contains the AP specific settings
type ActivityPub struct {
	DeliveryDepth   int        `yaml:"delivery-depth" default:"5"`
	ForwardingDepth int        `yaml:"forwarding-depth" default:"5"`
	RequestTimeout  int        `yaml:"timeout" default:"30"`
	DeliveryRetries int        `yaml:"retries" default:"10"`
	Ratelimit       rate.Limit `yaml:"ratelimit" default:"10"`
	Ratebucket      int        `yaml:"ratebucket" default:"1000"`
}

// RequestTimeoutDuration converts the request timneout int into a time.Duration
func (ap *ActivityPub) RequestTimeoutDuration() time.Duration {
	return time.Duration(ap.RequestTimeout) * time.Second
}
