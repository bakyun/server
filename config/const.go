package config

// Application specific constants
const (
	ApplicationName = "bakyun"
)

// OAuth data
const (
	URISeparator = ";"
	OBB          = "urn:ietf:wg:oauth:2.0:oob"
)

// provided by buildscript
var (
	GitCommit   = "unknown"
	GitBranch   = "none"
	GitState    = "dirty"
	GitSummary  = "dirty"
	BuildDate   = "now"
	Version     = "0.0.0"
	LongVersion = "0.0.0-dirty"
)

// supported feature set
var (
	FeaturesSupported = []string{
		"pleroma_api",
		"mastodon_api",
		"mastodon_api_streaming",
		"chat",
		"relay",
		"ap_server",
		"ap_client",
	}

	RestrictedNicknames = []string{
		".well-known",
		"~",
	}
)
