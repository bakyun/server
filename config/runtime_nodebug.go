//+build !debug

package config

const (
	// Debug defines if the application is built in debug mode
	Debug = false
)
